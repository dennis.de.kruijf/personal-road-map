﻿using System;
using System.Collections.Generic;
using System.Linq;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;

namespace Seeder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start seeding");

            using (var context = new PersonalRoadMapDbContext())
            {
                context.Database.EnsureCreated();

                var beheren1 = new Competence() { CompetenceName = "Beheren", Level = 1 };
                var testen1 = new Competence() { CompetenceName = "Testen", Level = 1 };
                var realiseren1 = new Competence() { CompetenceName = "Realiseren", Level = 1 };
                var list1 = new List<Competence> { beheren1, testen1, realiseren1 };

                context.Competences.AddRange(list1);


                var beheren2 = new Competence() { CompetenceName = "Beheren", Level = 2 };
                var testen2 = new Competence() { CompetenceName = "Testen", Level = 2 };
                var realiseren2 = new Competence() { CompetenceName = "Realiseren", Level = 2 };

                var list2 = new List<Competence> { beheren2, testen2, realiseren2 };

                context.Competences.AddRange(list2);

                var beheren3 = new Competence() { CompetenceName = "Beheren", Level = 3 };
                var testen3 = new Competence() { CompetenceName = "Testen", Level = 3 };
                var realiseren3 = new Competence() { CompetenceName = "Realiseren", Level = 3 };

                var list3 = new List<Competence> { beheren3, testen3, realiseren3 };

                context.Competences.AddRange(list3);

                Console.WriteLine("Added competences");


                var moduleList = new List<Module>();
                var studentModule = new Module();


                for (int i = 0; i < 4; i++)
                {
                    var year = new AcademicYear()
                    {
                        Id = new Guid(),
                        Year = i + 1,
                        Description = "Een jaar"
                    };
                    var yearSemesters = new List<Semester>();

                    for (int j = 0; j < 2; j++)
                    {
                        var Semester = new Semester()
                        {
                            Id = new Guid(),
                            Year = year
                        };

                        for (int k = 0; k < 10; k++)
                        {
                            var module = new Module()
                            {
                                Id = new Guid(),
                                EC = 30,
                                Description = "I am a module",
                                Name = "Module"
                            };

                            context.SemesterModules.Add(new SemesterModule()
                            { Module = module, ModuleId = module.Id, Semester = Semester, SemesterId = Semester.Id });

                            if (i == 0)
                            {
                                var moduleAchievementBeheren1 = new CompetenceModuleAchievement() { Competence = beheren1, CompetenceId = beheren1.Id, Module = module, ModuleId = module.Id };
                                var moduleAchievementTesten1 = new CompetenceModuleAchievement() { Competence = testen1, CompetenceId = testen1.Id, Module = module, ModuleId = module.Id };
                                var moduleAchievementRealiseren1 = new CompetenceModuleAchievement() { Competence = realiseren1, CompetenceId = realiseren1.Id, Module = module, ModuleId = module.Id };

                                context.ModuleAchievements.AddRange(moduleAchievementBeheren1, moduleAchievementTesten1, moduleAchievementRealiseren1);
                            }

                            else if (i == 1)
                            {
                                var moduleRequirementBeheren1 = new CompetanceModuleRequirement() { Competence = beheren1, CompetenceId = beheren1.Id, Module = module, ModuleId = module.Id };
                                var moduleRequirementTesten1 = new CompetanceModuleRequirement() { Competence = testen1, CompetenceId = testen1.Id, Module = module, ModuleId = module.Id };
                                var moduleRequirementRealiseren1 = new CompetanceModuleRequirement() { Competence = realiseren1, CompetenceId = realiseren1.Id, Module = module, ModuleId = module.Id };

                                var moduleAchievementBeheren2 = new CompetenceModuleAchievement() { Competence = beheren2, CompetenceId = beheren2.Id, Module = module, ModuleId = module.Id };
                                var moduleAchievementTesten2 = new CompetenceModuleAchievement() { Competence = testen2, CompetenceId = testen2.Id, Module = module, ModuleId = module.Id };
                                var moduleAchievementRealiseren2 = new CompetenceModuleAchievement() { Competence = realiseren2, CompetenceId = realiseren2.Id, Module = module, ModuleId = module.Id };

                                context.ModuleAchievements.AddRange(moduleAchievementBeheren2, moduleAchievementTesten2, moduleAchievementRealiseren2);
                                context.ModuleRequirements.AddRange(moduleRequirementBeheren1, moduleRequirementTesten1, moduleRequirementRealiseren1);


                            }
                            else if (i == 2)
                            {
                                var moduleRequirementBeheren2 = new CompetanceModuleRequirement() { Competence = beheren2, CompetenceId = beheren2.Id, Module = module, ModuleId = module.Id };
                                var moduleRequirementTesten2 = new CompetanceModuleRequirement() { Competence = testen2, CompetenceId = testen2.Id, Module = module, ModuleId = module.Id };
                                var moduleRequirementRealiseren2 = new CompetanceModuleRequirement() { Competence = realiseren2, CompetenceId = realiseren2.Id, Module = module, ModuleId = module.Id };

                                context.ModuleRequirements.AddRange(moduleRequirementBeheren2, moduleRequirementTesten2, moduleRequirementRealiseren2);


                                var moduleAchievementBeheren3 = new CompetenceModuleAchievement() { Competence = beheren3, CompetenceId = realiseren3.Id, Module = module, ModuleId = module.Id };
                                var moduleAchievementTesten3 = new CompetenceModuleAchievement() { Competence = testen3, CompetenceId = testen3.Id, Module = module, ModuleId = module.Id };
                                var moduleAchievementRealiseren3 = new CompetenceModuleAchievement() { Competence = realiseren3, CompetenceId = realiseren3.Id, Module = module, ModuleId = module.Id };

                                context.ModuleAchievements.AddRange(moduleAchievementBeheren3, moduleAchievementTesten3, moduleAchievementRealiseren3);
                            }
                            else
                            {

                                var moduleRequirementBeheren3 = new CompetanceModuleRequirement() { Competence = beheren3, CompetenceId = beheren3.Id, Module = module, ModuleId = module.Id };
                                var moduleRequirementTesten3 = new CompetanceModuleRequirement() { Competence = testen3, CompetenceId = testen3.Id, Module = module, ModuleId = module.Id };
                                var moduleRequirementRealiseren3 = new CompetanceModuleRequirement() { Competence = realiseren3, CompetenceId = realiseren3.Id, Module = module, ModuleId = module.Id };


                                context.ModuleRequirements.AddRange(moduleRequirementBeheren3, moduleRequirementTesten3, moduleRequirementRealiseren3);

                                var moduleAchievementBeheren3 = new CompetenceModuleAchievement() { Competence = beheren3, CompetenceId = realiseren3.Id, Module = module, ModuleId = module.Id };
                                var moduleAchievementTesten3 = new CompetenceModuleAchievement() { Competence = testen3, CompetenceId = testen3.Id, Module = module, ModuleId = module.Id };
                                var moduleAchievementRealiseren3 = new CompetenceModuleAchievement() { Competence = realiseren3, CompetenceId = realiseren3.Id, Module = module, ModuleId = module.Id };

                                context.ModuleAchievements.AddRange(moduleAchievementBeheren3, moduleAchievementTesten3, moduleAchievementRealiseren3);


                            }


                            studentModule = module;

                            context.Modules.Add(module);
                        }
                        yearSemesters.Add(Semester);
                        context.Semesters.Add(Semester);
                        moduleList.Add(studentModule);
                    }

                    year.Semesters = yearSemesters;
                    context.AcademicYears.Add(year);
                }

                Console.WriteLine("adding students");

                for (var x = 0; x < 10; x++)
                {
                    var roadmap = new RoadMap() { Id = Guid.NewGuid() };

                    context.RoadMaps.Add(roadmap);

                    foreach (var module in moduleList)
                    {
                        var roadmapModule = new ModuleRoadMap() { Module = module, ModuleId = module.Id, RoadMap = roadmap, RoadMapId = roadmap.Id };

                        context.ModuleRoadMaps.Add(roadmapModule);
                    }



                    var student = new Student() { Email = "test@windesheim.nl", Name = "Piet van test", Id = Guid.NewGuid(), RoadMap = roadmap };

                    context.Students.Add(student);

                }


                context.SaveChanges();
            }

            Console.WriteLine("Done seeding");
        }
    }
}