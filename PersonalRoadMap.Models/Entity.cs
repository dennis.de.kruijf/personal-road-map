﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PersonalRoadMap.Entities
{
	public class Entity
	{
		[Key]
		public Guid Id { get; set; }
	}
}
