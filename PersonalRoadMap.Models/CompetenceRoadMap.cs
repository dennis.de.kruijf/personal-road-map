﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PersonalRoadMap.Entities
{
	public class CompetenceRoadMap
	{
		public Competence Competence { get; set; }
		public Guid CompetenceId { get; set; }
		
		[JsonIgnore]
		public RoadMap RoadMap { get; set; }
		
		public Guid RoadMapId { get; set; }
	}
}
