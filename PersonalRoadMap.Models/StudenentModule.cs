﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonalRoadMap.Entities
{
	public class StudenentModule
	{
		public Student Student { get; set; }
		public Guid StudentId { get; set; }
		public Module Module { get; set; }
		public Guid ModuleId { get; set; }
	}
}
