using System.Collections;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace PersonalRoadMap.Entities
{
    public class Semester : Entity
    {
        [JsonIgnore]
        public AcademicYear Year { get; set; }

        public IEnumerable<SemesterModule> Modules { get; set; }
    }
}