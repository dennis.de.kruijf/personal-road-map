﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace PersonalRoadMap.Entities
{
	public class Student : Entity
	{
		public string Email { get; set; }
		public string Name { get; set; }
		public IEnumerable<Module> Modules { get; set; }
		
		public Guid RoadMapId { get; set; }
		public RoadMap RoadMap { get; set; }
	}
}
