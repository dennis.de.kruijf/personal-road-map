﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PersonalRoadMap.Entities
{
	public class CompetanceModuleRequirement
	{
		[JsonIgnore]
		public Competence Competence { get; set; }
		public Guid CompetenceId { get; set; }
		public Module Module { get; set; }
		public Guid ModuleId { get; set; }
	}
}
