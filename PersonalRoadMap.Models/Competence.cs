﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PersonalRoadMap.Entities
{
	public class Competence : Entity
	{
        [Required]
		public string CompetenceName { get; set; }
        [Required]
		public int Level { get; set; }
		
		[JsonIgnore]
		public IEnumerable<CompetenceModuleAchievement> ModuleAchievements { get; set; }
		
		[JsonIgnore]
		public IEnumerable<CompetanceModuleRequirement> ModuleRequirements { get; set; }
		
		[JsonIgnore]
		public IEnumerable<CompetenceRoadMap> RoadMapsRequirements { get; set; }
	}
}
