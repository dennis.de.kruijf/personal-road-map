using System.Collections;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace PersonalRoadMap.Entities
{
    public class AcademicYear : Entity
    {
        public int Year { get; set; }
        public string Description { get; set; }

        public IEnumerable<Semester> Semesters { get; set; }
    }
}