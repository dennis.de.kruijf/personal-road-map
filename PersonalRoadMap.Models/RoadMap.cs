﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PersonalRoadMap.Entities
{
	public class RoadMap : Entity
	{
		public IEnumerable<CompetenceRoadMap> Requirements { get; set; }
		
		public IEnumerable<ModuleRoadMap> Modules { get; set; }

		public bool CompareAchievementsWithRequirements(IEnumerable<Competence> requirementsList, IEnumerable<Competence> achievementList)
		{
			return requirementsList.Except(achievementList).ToList().Count.Equals(0);
		}
	}
}
