using System;
using System.Text.Json.Serialization;

namespace PersonalRoadMap.Entities
{
    public class SemesterModule
    {
        [JsonIgnore]
        public Guid SemesterId { get; set; }
        
        [JsonIgnore]
        public Semester Semester { get; set; }
        
        [JsonIgnore]
        public Guid ModuleId { get; set; }

//        [JsonIgnore]
        public Module Module { get; set; }
    }
}