using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PersonalRoadMap.Entities
{
	public class Module : Entity
	{
		public int EC { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }

		[JsonIgnore]
		public IEnumerable<SemesterModule> Semesters { get; set; }
		
		[JsonIgnore]
		public IEnumerable<CompetanceModuleRequirement> Requirements { get; set; }
		
		[JsonIgnore]
		public IEnumerable<CompetenceModuleAchievement> Achievements { get; set; }
		
		[JsonIgnore]
		public IEnumerable<ModuleRoadMap> RoadMaps { get; set; }
	}
}
