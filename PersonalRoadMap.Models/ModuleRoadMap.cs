using System;
using System.Text.Json.Serialization;

namespace PersonalRoadMap.Entities
{
    public class ModuleRoadMap
    {
        public Module Module { get; set; }
        public Guid ModuleId { get; set; }
        
        [JsonIgnore]
        public RoadMap RoadMap { get; set; }
        public Guid RoadMapId { get; set; }
    }
}