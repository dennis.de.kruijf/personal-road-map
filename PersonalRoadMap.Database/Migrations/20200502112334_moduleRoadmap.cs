﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PersonalRoadMap.Database.Migrations
{
    public partial class moduleRoadmap : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CompetenceName",
                table: "Competences",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "ModuleRoadMap",
                columns: table => new
                {
                    ModuleId = table.Column<Guid>(nullable: false),
                    RoadMapId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleRoadMap", x => new { x.ModuleId, x.RoadMapId });
                    table.ForeignKey(
                        name: "FK_ModuleRoadMap_Modules_ModuleId",
                        column: x => x.ModuleId,
                        principalTable: "Modules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleRoadMap_RoadMaps_RoadMapId",
                        column: x => x.RoadMapId,
                        principalTable: "RoadMaps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ModuleRoadMap_RoadMapId",
                table: "ModuleRoadMap",
                column: "RoadMapId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ModuleRoadMap");

            migrationBuilder.AlterColumn<string>(
                name: "CompetenceName",
                table: "Competences",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
