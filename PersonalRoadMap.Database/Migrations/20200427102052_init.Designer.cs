﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PersonalRoadMap.Database;

namespace PersonalRoadMap.Database.Migrations
{
    [DbContext(typeof(PersonalRoadMapDbContext))]
    [Migration("20200427102052_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("PersonalRoadMap.Entities.AcademicYear", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Year")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("AcademicYear");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.CompetanceModuleRequirement", b =>
                {
                    b.Property<Guid>("CompetenceId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("ModuleId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("CompetenceId", "ModuleId");

                    b.HasIndex("ModuleId");

                    b.ToTable("ModuleRequirements");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.Competence", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("CompetenceName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Level")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Competences");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.CompetenceModuleAchievement", b =>
                {
                    b.Property<Guid>("CompetenceId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("ModuleId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("CompetenceId", "ModuleId");

                    b.HasIndex("ModuleId");

                    b.ToTable("ModuleAchievements");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.CompetenceRoadMap", b =>
                {
                    b.Property<Guid>("CompetenceId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("RoadMapId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("CompetenceId", "RoadMapId");

                    b.HasIndex("RoadMapId");

                    b.ToTable("RoadMapAchievements");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.Module", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("EC")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid?>("StudentId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("StudentId");

                    b.ToTable("Modules");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.RoadMap", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.ToTable("RoadMaps");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.Semester", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid?>("YearId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("YearId");

                    b.ToTable("Semester");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.SemesterModule", b =>
                {
                    b.Property<Guid>("SemesterId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("ModuleId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("SemesterId", "ModuleId");

                    b.HasIndex("ModuleId");

                    b.ToTable("SemesterModules");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.Student", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid?>("RoadMapId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("RoadMapId");

                    b.ToTable("Students");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.CompetanceModuleRequirement", b =>
                {
                    b.HasOne("PersonalRoadMap.Entities.Competence", "Competence")
                        .WithMany("ModuleRequirements")
                        .HasForeignKey("CompetenceId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("PersonalRoadMap.Entities.Module", "Module")
                        .WithMany("Requirements")
                        .HasForeignKey("ModuleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.CompetenceModuleAchievement", b =>
                {
                    b.HasOne("PersonalRoadMap.Entities.Competence", "Competence")
                        .WithMany("ModuleAchievements")
                        .HasForeignKey("CompetenceId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("PersonalRoadMap.Entities.Module", "Module")
                        .WithMany("Achievements")
                        .HasForeignKey("ModuleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.CompetenceRoadMap", b =>
                {
                    b.HasOne("PersonalRoadMap.Entities.Competence", "Competence")
                        .WithMany("RoadMapsRequirements")
                        .HasForeignKey("CompetenceId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("PersonalRoadMap.Entities.RoadMap", "RoadMap")
                        .WithMany("Requirements")
                        .HasForeignKey("RoadMapId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.Module", b =>
                {
                    b.HasOne("PersonalRoadMap.Entities.Student", null)
                        .WithMany("Modules")
                        .HasForeignKey("StudentId");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.Semester", b =>
                {
                    b.HasOne("PersonalRoadMap.Entities.AcademicYear", "Year")
                        .WithMany("Semesters")
                        .HasForeignKey("YearId");
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.SemesterModule", b =>
                {
                    b.HasOne("PersonalRoadMap.Entities.Module", "Module")
                        .WithMany("Semesters")
                        .HasForeignKey("ModuleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("PersonalRoadMap.Entities.Semester", "Semester")
                        .WithMany("Modules")
                        .HasForeignKey("SemesterId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("PersonalRoadMap.Entities.Student", b =>
                {
                    b.HasOne("PersonalRoadMap.Entities.RoadMap", "RoadMap")
                        .WithMany()
                        .HasForeignKey("RoadMapId");
                });
#pragma warning restore 612, 618
        }
    }
}
