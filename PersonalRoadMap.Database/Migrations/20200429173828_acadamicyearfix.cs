﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PersonalRoadMap.Database.Migrations
{
    public partial class acadamicyearfix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Semester_AcademicYear_YearId",
                table: "Semester");

            migrationBuilder.DropForeignKey(
                name: "FK_SemesterModules_Semester_SemesterId",
                table: "SemesterModules");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Semester",
                table: "Semester");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AcademicYear",
                table: "AcademicYear");

            migrationBuilder.RenameTable(
                name: "Semester",
                newName: "Semesters");

            migrationBuilder.RenameTable(
                name: "AcademicYear",
                newName: "AcademicYears");

            migrationBuilder.RenameIndex(
                name: "IX_Semester_YearId",
                table: "Semesters",
                newName: "IX_Semesters_YearId");

            migrationBuilder.AlterColumn<string>(
                name: "CompetenceName",
                table: "Competences",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Semesters",
                table: "Semesters",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AcademicYears",
                table: "AcademicYears",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_SemesterModules_Semesters_SemesterId",
                table: "SemesterModules",
                column: "SemesterId",
                principalTable: "Semesters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Semesters_AcademicYears_YearId",
                table: "Semesters",
                column: "YearId",
                principalTable: "AcademicYears",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SemesterModules_Semesters_SemesterId",
                table: "SemesterModules");

            migrationBuilder.DropForeignKey(
                name: "FK_Semesters_AcademicYears_YearId",
                table: "Semesters");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Semesters",
                table: "Semesters");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AcademicYears",
                table: "AcademicYears");

            migrationBuilder.RenameTable(
                name: "Semesters",
                newName: "Semester");

            migrationBuilder.RenameTable(
                name: "AcademicYears",
                newName: "AcademicYear");

            migrationBuilder.RenameIndex(
                name: "IX_Semesters_YearId",
                table: "Semester",
                newName: "IX_Semester_YearId");

            migrationBuilder.AlterColumn<string>(
                name: "CompetenceName",
                table: "Competences",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Semester",
                table: "Semester",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AcademicYear",
                table: "AcademicYear",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Semester_AcademicYear_YearId",
                table: "Semester",
                column: "YearId",
                principalTable: "AcademicYear",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SemesterModules_Semester_SemesterId",
                table: "SemesterModules",
                column: "SemesterId",
                principalTable: "Semester",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
