﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PersonalRoadMap.Database.Migrations
{
    public partial class dbsetRoadmapModules : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModuleRoadMap_Modules_ModuleId",
                table: "ModuleRoadMap");

            migrationBuilder.DropForeignKey(
                name: "FK_ModuleRoadMap_RoadMaps_RoadMapId",
                table: "ModuleRoadMap");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ModuleRoadMap",
                table: "ModuleRoadMap");

            migrationBuilder.RenameTable(
                name: "ModuleRoadMap",
                newName: "ModuleRoadMaps");

            migrationBuilder.RenameIndex(
                name: "IX_ModuleRoadMap_RoadMapId",
                table: "ModuleRoadMaps",
                newName: "IX_ModuleRoadMaps_RoadMapId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ModuleRoadMaps",
                table: "ModuleRoadMaps",
                columns: new[] { "ModuleId", "RoadMapId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ModuleRoadMaps_Modules_ModuleId",
                table: "ModuleRoadMaps",
                column: "ModuleId",
                principalTable: "Modules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ModuleRoadMaps_RoadMaps_RoadMapId",
                table: "ModuleRoadMaps",
                column: "RoadMapId",
                principalTable: "RoadMaps",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModuleRoadMaps_Modules_ModuleId",
                table: "ModuleRoadMaps");

            migrationBuilder.DropForeignKey(
                name: "FK_ModuleRoadMaps_RoadMaps_RoadMapId",
                table: "ModuleRoadMaps");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ModuleRoadMaps",
                table: "ModuleRoadMaps");

            migrationBuilder.RenameTable(
                name: "ModuleRoadMaps",
                newName: "ModuleRoadMap");

            migrationBuilder.RenameIndex(
                name: "IX_ModuleRoadMaps_RoadMapId",
                table: "ModuleRoadMap",
                newName: "IX_ModuleRoadMap_RoadMapId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ModuleRoadMap",
                table: "ModuleRoadMap",
                columns: new[] { "ModuleId", "RoadMapId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ModuleRoadMap_Modules_ModuleId",
                table: "ModuleRoadMap",
                column: "ModuleId",
                principalTable: "Modules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ModuleRoadMap_RoadMaps_RoadMapId",
                table: "ModuleRoadMap",
                column: "RoadMapId",
                principalTable: "RoadMaps",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
