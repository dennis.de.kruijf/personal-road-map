﻿using Microsoft.EntityFrameworkCore;
using PersonalRoadMap.Entities;
using System;
using System.Collections.Generic;

namespace PersonalRoadMap.Database
{
	public class PersonalRoadMapDbContext : DbContext
	{
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			//optionsBuilder.UseSqlServer("Data Source=127.0.0.1,5434;Initial Catalog=PersonalRoadMap;User ID=sa;Password=Pass@word;");
//			optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=PersonalRoadMap;Trusted_Connection=True;");

			//Connection string graag laten staan, die gebruik ik - Demiën
 			//optionsBuilder.UseSqlServer("Data Source=192.168.2.175,5434;Initial Catalog=PersonalRoadMap;User ID=sa;Password=Pass@word;");
			//optionsBuilder.UseSqlServer("Data Source=192.168.43.230,5434;Initial Catalog=PersonalRoadMap;User ID=sa;Password=Pass@word;");
 			optionsBuilder.UseSqlServer("Data Source=192.168.2.158,5434;Initial Catalog=PersonalRoadMap;User ID=sa;Password=Pass@word;");
			//optionsBuilder.UseSqlServer("Data Source=192.168.43.230,5434;Initial Catalog=PersonalRoadMap;User ID=sa;Password=Pass@word;");
			//optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=PersonalRoadMap;Trusted_Connection=True;");
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<CompetanceModuleRequirement>()
			    .HasKey(c => new { c.CompetenceId, c.ModuleId });
			modelBuilder.Entity<CompetanceModuleRequirement>()
				.HasOne( comp=> comp.Competence)
				.WithMany(b => b.ModuleRequirements)
				.HasForeignKey(bc => bc.CompetenceId);
			modelBuilder.Entity<CompetanceModuleRequirement>()
				.HasOne(bc => bc.Module)
				.WithMany(c => c.Requirements)
				.HasForeignKey(bc => bc.ModuleId);

			modelBuilder.Entity<CompetenceModuleAchievement>()
  			    .HasKey(c => new { c.CompetenceId, c.ModuleId });
			modelBuilder.Entity<CompetenceModuleAchievement>()
				.HasOne(comp => comp.Competence)
				.WithMany(b => b.ModuleAchievements)
				.HasForeignKey(bc => bc.CompetenceId);
			modelBuilder.Entity<CompetenceModuleAchievement>()
				.HasOne(bc => bc.Module)
				.WithMany(c => c.Achievements)
				.HasForeignKey(bc => bc.ModuleId);

			modelBuilder.Entity<CompetenceRoadMap>()
			   .HasKey(c => new { c.CompetenceId, c.RoadMapId });
			modelBuilder.Entity<CompetenceRoadMap>()
				.HasOne(comp => comp.Competence)
				.WithMany(b => b.RoadMapsRequirements)
				.HasForeignKey(bc => bc.CompetenceId);
			modelBuilder.Entity<CompetenceRoadMap>()
				.HasOne(bc => bc.RoadMap)
				.WithMany(c => c.Requirements)
				.HasForeignKey(bc => bc.RoadMapId);
			
			
			modelBuilder.Entity<ModuleRoadMap>()
				.HasKey(c => new { c.ModuleId, c.RoadMapId });
			modelBuilder.Entity<ModuleRoadMap>()
				.HasOne(comp => comp.Module)
				.WithMany(b => b.RoadMaps)
				.HasForeignKey(bc => bc.ModuleId);
			modelBuilder.Entity<ModuleRoadMap>()
				.HasOne(bc => bc.RoadMap)
				.WithMany(c => c.Modules)
				.HasForeignKey(bc => bc.RoadMapId);


			modelBuilder.Entity<SemesterModule>()
			   .HasKey(c => new { c.SemesterId, c.ModuleId });
			modelBuilder.Entity<SemesterModule>()
				.HasOne(sem => sem.Semester)
				.WithMany(m => m.Modules)
				.HasForeignKey(bc => bc.SemesterId);
			modelBuilder.Entity<SemesterModule>()
				.HasOne(bc => bc.Module)
				.WithMany(c => c.Semesters)
				.HasForeignKey(bc => bc.ModuleId);
		}

		public DbSet<Student> Students { get; set; }
		public DbSet<Competence> Competences { get; set; }
		public DbSet<Module> Modules { get; set; }
		public DbSet<RoadMap> RoadMaps { get; set; }
		public DbSet<AcademicYear> AcademicYears { get; set; }
		public DbSet<Semester> Semesters { get; set; }
		public DbSet<CompetanceModuleRequirement> ModuleRequirements { get; set; }
		public DbSet<CompetenceModuleAchievement> ModuleAchievements { get; set; }
		public DbSet<SemesterModule> SemesterModules { get; set; }
		public DbSet<CompetenceRoadMap> RoadMapAchievements { get; set; }
		public DbSet<ModuleRoadMap> ModuleRoadMaps { get; set; }
	}
}
