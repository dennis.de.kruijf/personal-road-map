using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Text.Json;
using PersonalRoadMap.Entities;

namespace PersonalRoadMap.Tests.Integration
{
    [TestFixture]
    public class RoadMapControllerIntegrationTest
    {
        private APIWebApplicationFactory _factory;
        private HttpClient _client;
        
        [OneTimeSetUp]
        public void GivenARequestToTheController()
        {
            _factory = new APIWebApplicationFactory();
            _client = _factory.CreateClient();
        }
        
        [Test]
        public async Task AddRoadMapRequestResultIsOk()
        {
            //Arrange
            var body = new {};
            var post = new StringContent(JsonSerializer.Serialize(body));
            post.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            
            //Act
            var result = await _client.PostAsync("/api/roadmap", post);

            //Assert
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.Created));
        }

        [Test]
        public async Task DeleteRoadMapRequestResultIsOk()
        {
            var body = new {};
            var post = new StringContent(JsonSerializer.Serialize(body));
            post.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var postResult = await _client.PostAsync("/api/roadmap", post);
            var response = JsonSerializer.Deserialize<RoadMap>(await postResult.Content.ReadAsStringAsync());

            //Act
            var deleteResult = await _client.DeleteAsync("/api/roadmap/" + response.Id);
            
            Assert.That(deleteResult.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            _client.Dispose();
            _factory.Dispose();
        }
    }
}