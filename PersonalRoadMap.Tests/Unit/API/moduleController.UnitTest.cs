﻿using NUnit.Framework;
using PersonalRoadMap.API.Controllers;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;

using System;
using System.Collections.Generic;
using System.Text;
using PersonalRoadMap.API.Services;

namespace PersonalRoadMap.Tests.Unit.API
{
    class moduleControllerUnitTest
    {

        [SetUp]
        public void Setup()
        {
        }
        //gets exisiting modules
        [Test]
        public void GetModules()
        {
            var context = new PersonalRoadMapDbContext();
            var modelService = new ModuleService();
            var ModuleController = new ModuleController(context, modelService);

            var modules = ModuleController.GetModules();

            Assert.IsNotNull(modules);
        }
        //creates a new module
        [Test]
        public void createModule()
        {
            var context = new PersonalRoadMapDbContext();
            var modelService = new ModuleService();
            var ModuleController = new ModuleController(context, modelService);
            var module = new Module
            {
                Id = Guid.NewGuid(),
                Name = "UnitTest"
            };

            var modules = ModuleController.PostModule(module);

            var getModule = ModuleController.GetModule(module.Id);
            Assert.IsNotNull(getModule);
        }
        //deletes a module
        [Test]
        public void deleteModule()
        {
            var context = new PersonalRoadMapDbContext();
            var modelService = new ModuleService();
            var ModuleController = new ModuleController(context, modelService);

            var module = new Module
            {
                Id = Guid.NewGuid(),
                Name = "DeleteUnitTest"
            };
            var modules = ModuleController.PostModule(module);
            var delmodules = ModuleController.DeleteModule(module.Id);
            var getModule = ModuleController.GetModule(module.Id);


            Assert.IsNull(getModule);
        }
    }
}
