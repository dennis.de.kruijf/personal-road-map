using System.Collections.Generic;
using NUnit.Framework;
using PersonalRoadMap.Entities;

namespace PersonalRoadMap.Tests.Unit.API
{
    public class RoadMapControllerUnitTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestCompareRequirementsWithAchievementsReturnsTrue()
        {
            //Arrange
            var CompetenceList = new List<Competence>();
            CompetenceList.Add(new Competence() {CompetenceName = "Software engineering", Level = 1});
            CompetenceList.Add(new Competence() {CompetenceName = "Software engineering", Level = 2});
            CompetenceList.Add(new Competence() {CompetenceName = "Software engineering", Level = 3});
            
            var roadmap = new RoadMap();
            
            //Act
            var isValidRoadMap = roadmap.CompareAchievementsWithRequirements(CompetenceList, CompetenceList);
            
            //Assert
            Assert.IsTrue(isValidRoadMap);
        }
        
        [Test]
        public void TestCompareRequirementsWithAchievementsReturnsFalse()
        {
            //Arrange
            var competence1 = new Competence() {CompetenceName = "Software engineering", Level = 1};
            var competence2 = new Competence() {CompetenceName = "Software engineering", Level = 2};
            var competence3 = new Competence() {CompetenceName = "Software engineering", Level = 3};
            
            var RequirementsList = new List<Competence>();
            RequirementsList.Add(competence1);
            RequirementsList.Add(competence2);
            RequirementsList.Add(competence3);

            var AchievementsList = new List<Competence>();
            AchievementsList.Add(competence1);
            AchievementsList.Add(competence2);
            
            var roadmap = new RoadMap();
            
            //Act
            var isValidRoadMap = roadmap.CompareAchievementsWithRequirements(RequirementsList, AchievementsList);
            
            //Assert
            Assert.IsFalse(isValidRoadMap);
        }

        [Test]
        public void TestIsRoadMapReturnsTrue()
        {
            
        }
        
    }
}