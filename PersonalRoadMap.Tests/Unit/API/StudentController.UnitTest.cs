﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using PersonalRoadMap.API.Controllers;
using PersonalRoadMap.API.Services;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;

namespace PersonalRoadMap.Tests.Unit.API
{
    public class StudentController_UnitTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task TestCreateStudentOnNonExistent()
        {
            //Arrange
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, "Test Student"),
                new Claim(ClaimTypes.Name, "teststudent@student.windesheim.nl"),
                new Claim(ClaimTypes.Email, "teststudent@student.windesheim.nl")
            }));       
            
            
            var context = new PersonalRoadMapDbContext();
            var studentService = new StudentService();
            
            var controller = new StudentController(context, studentService);
            
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext { User = user }
            };

            //Act
            var result = await controller.GetStudent();
            var id = result.Value.Id;
            var email = result.Value.Email;

            //Assert
            Assert.AreEqual(email, "teststudent@student.windesheim.nl");
            Assert.NotNull(id);
            
            //Cleanup
            controller.DeleteStudent(id);
        }

        [Test]
        public void GetStudents()
        {
            var context = new PersonalRoadMapDbContext();
            var studentService = new StudentService();
            var studentController = new StudentController(context, studentService);

            var students = studentController.Get();

            Assert.IsNotNull(students);
        }
    }
}  