using Microsoft.AspNetCore.Authorization;

namespace PersonalRoadMap.API.Authorization
{
    public class StudentOrStudentbegeleiderAuthorizationPolicy
    {
        public static string Name => "StudentOrStudentbegeleider";
 
        public static void Build(AuthorizationPolicyBuilder builder) =>
            builder.RequireClaim("groups", "7c954ce4-3aa6-4c75-a71f-ea638222431d", "f22160a1-413c-4988-9d2e-d39cedc955e5");  
    }
}