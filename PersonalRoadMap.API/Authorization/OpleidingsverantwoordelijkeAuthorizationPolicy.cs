using Microsoft.AspNetCore.Authorization;

namespace PersonalRoadMap.API.Authorization
{
    public class OpleidingsverantwoordelijkeAuthorizationPolicy
    {
        public static string Name => "Opleidingsverantwoordelijke";
 
        public static void Build(AuthorizationPolicyBuilder builder) =>
            builder.RequireClaim("groups", "ec11b268-1c7a-4037-8736-0637140fa7ef");  
    }
}