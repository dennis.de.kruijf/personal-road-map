using Microsoft.AspNetCore.Authorization;

namespace PersonalRoadMap.API.Authorization
{
    public class StudentbegeleiderAuthorizationPolicy
    {
        public static string Name => "Studentbegeleider";
 
        public static void Build(AuthorizationPolicyBuilder builder) =>
            builder.RequireClaim("groups", "f22160a1-413c-4988-9d2e-d39cedc955e5");  
    }
}