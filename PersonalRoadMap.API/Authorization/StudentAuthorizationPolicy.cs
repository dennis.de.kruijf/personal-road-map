using Microsoft.AspNetCore.Authorization;

namespace PersonalRoadMap.API.Authorization
{
    public class StudentAuthorizationPolicy
    {
        public static string Name => "Student";
 
        public static void Build(AuthorizationPolicyBuilder builder) =>
            builder.RequireClaim("groups", "7c954ce4-3aa6-4c75-a71f-ea638222431d");  
    }
}