﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PersonalRoadMap.API.Services;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;

namespace PersonalRoadMap.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly PersonalRoadMapDbContext _context;
        private readonly IStudentService _StudentService;

        public StudentController(PersonalRoadMapDbContext context, IStudentService studentService)
        {
            _context = context;
            _StudentService = studentService;
        }

        // GET: api/Student
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<Student>> GetStudent()
        {
            string email = User.FindFirstValue(ClaimTypes.Email);

            if (StudentExists(email))
            {
                return await GetStudent(email);
            }

            var newStudent = new Student();
          
            newStudent.Email = email;
            newStudent.Name = User.FindFirstValue(ClaimTypes.Name);

            return await PostStudent(newStudent);
        }

        // GET: api/Student/5
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<Student>> GetStudent(Guid id)
        {
            var student = await _StudentService.GetStudent(id).ConfigureAwait(false);

            if (student == null)
            {
                return NotFound();
            }

            return student;
        }

        [Authorize("Opleidingsverantwoordelijke")]
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<Student>>> GetAll()
        {
            try
            {
                var students = await _StudentService.GetStudents().ConfigureAwait(false);

                return Ok(students);
            }
            catch (Exception e)
            {
                return null;
            }


        }

        // GET: api/Student/5
        [Authorize]
        [HttpGet("{email}")]
        [Authorize("Opleidingsverantwoordelijke")]
        public async Task<ActionResult<Student>> GetStudent(string email)
        {
            var student = await _context.Students
                .Where(s => s.Email == email)
                .Include(r => r.RoadMap)
                .FirstAsync();

            if (student == null)
            {
                return NotFound();
            }

            return student;
        }

        // PUT: api/Student/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStudent(Guid id, Student student)
        {
            if (id != student.Id)
            {
                return BadRequest();
            }
            
            _context.Entry(student).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Student
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize("Opleidingsverantwoordelijke")]
        [HttpPost]
        public async Task<ActionResult<Student>> PostStudent(Student student)
        {
            var rm = new RoadMap();
            _context.RoadMaps.Add(rm);

            student.RoadMapId = rm.Id;
            student.RoadMap = rm;
            
            _context.Students.Add(student);
            await _context.SaveChangesAsync();

            return await GetStudent(student.Email);
        }

        // DELETE: api/Student/5
        [Authorize("Opleidingsverantwoordelijke")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Student>> DeleteStudent(Guid id)
        {
            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            _context.Students.Remove(student);
            await _context.SaveChangesAsync();

            return student;
        }

        private bool StudentExists(Guid id)
        {
            return _context.Students.Any(e => e.Id == id);
        }
        
        private bool StudentExists(string email)
        {
            return _context.Students.Any(e => e.Email == email);
        }
    }
}
