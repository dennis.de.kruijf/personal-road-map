using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PersonalRoadMap.API.Services;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;

namespace PersonalRoadMap.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoadMapController : ControllerBase
    {

        private readonly IRoadMapService _RoadMapService;

        private readonly PersonalRoadMapDbContext _context;

        public RoadMapController(IRoadMapService roadMapService, PersonalRoadMapDbContext context)
        {
            _RoadMapService = roadMapService;
            _context = context;
        }

        // GET: api/RoadMap
        [Authorize("StudentOrStudentbegeleider")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoadMap>>> GetRoadMaps()
        {
            var roadMaps = await _context.RoadMaps
                .Include(map => map.Requirements)
                .ThenInclude(requirements => requirements.Competence)
                .Include(map => map.Modules)
                .ThenInclude(modules => modules.Module)
                .ToListAsync();
            
            return roadMaps;
        }

        // GET: api/RoadMap/5
        [Authorize("StudentOrStudentbegeleider")]
        [HttpGet("{id}")]
        public async Task<ActionResult<RoadMap>> GetRoadMap(Guid id)
        {
            var roadMap = await _context.RoadMaps
                .Include(map => map.Requirements)
                .ThenInclude(requirements => requirements.Competence)
                .Include(map => map.Modules)
                .ThenInclude(modules => modules.Module)
                .Where(map => map.Id.Equals(id))
                .FirstAsync();

            if (roadMap == null)
            {
                return NotFound();
            }

            return roadMap;
        }

        [Authorize("Studentbegeleider")]
        [HttpGet("GetByStudentId/{studentId}")]
        public async Task<ActionResult<RoadMap>> GetRoadMapByStudentIdAsync(Guid studentId)
        {
            var roadMap = await _RoadMapService.GetByStundentIdAsync(studentId).ConfigureAwait(false);

            if (roadMap == null)
            {
                return NotFound("roadmap not found");
            }

            return Ok(roadMap);
        }
        
        // POST: api/RoadMap/Validate
        [HttpPost("Validate")]
        public async Task<ActionResult> ValidateRoadMap(RoadMap roadMap)
        {
            var RequiredEC = 60;
            var totalEc = 0;
            
            var achievementList = new List<Competence>();
            var requirementsList = new List<Competence>();
            
            foreach (var module in roadMap.Modules)
            {
                var achievements = _context.ModuleAchievements
                    .Where(ma => ma.ModuleId.Equals(module.ModuleId))
                    .Include(cma => cma.Competence)
                    .Select(cma => cma.Competence).ToList();

                achievementList = achievementList.Concat(achievements).ToList();
                
                var requirements = _context.ModuleRequirements
                    .Where(ma => ma.ModuleId.Equals(module.ModuleId))
                    .Include(cma => cma.Competence).Select(cma => cma.Competence).ToList();
                
                requirementsList = requirementsList.Concat(requirements).ToList();
                
                totalEc += module.Module.EC;
            }
            
            return Ok(new
            {
                ec = totalEc >= RequiredEC,
                roadmap = roadMap.CompareAchievementsWithRequirements(requirementsList, achievementList)
            });
        }

        // PUT: api/RoadMap/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize("Student")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRoadMap(string id, List<Guid> roadMap)
        {
            var parsedId = Guid.Parse(id);
            
            var rm = await _context.RoadMaps.Where(r => r.Id == parsedId).FirstAsync();

            var existing = await _context.ModuleRoadMaps.Where(x => x.RoadMapId == rm.Id).ToListAsync();
            foreach (var match in existing)
            {
                _context.ModuleRoadMaps.Remove(match);
            }
            
            var modRmLs = new List<ModuleRoadMap>();

            foreach (var guid in roadMap)
            {
                var modRm = new ModuleRoadMap();
                modRm.ModuleId = guid;
                modRm.RoadMapId = rm.Id;
                modRmLs.Add(modRm);
            }

            rm.Modules = modRmLs;

            _context.Entry(rm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoadMapExists(Guid.Parse(id)))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RoadMap
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize("StudentOrStudentbegeleider")]
        [HttpPost]
        public async Task<ActionResult<RoadMap>> PostRoadMap(List<Guid> roadMap)
        {
            var rm = new RoadMap();
            var modRmLs = new List<ModuleRoadMap>();
            
            foreach (var guid in roadMap)
            {
                var modRm = new ModuleRoadMap();
                modRm.ModuleId = guid;
                modRm.RoadMapId = rm.Id;
                modRmLs.Add(modRm);
            }

            rm.Modules = modRmLs;
            
            _context.RoadMaps.Add(rm);
            await _context.SaveChangesAsync();

            return rm;
        }

        // DELETE: api/RoadMap/5
        [Authorize("Studentbegeleider")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<RoadMap>> DeleteRoadMap(Guid id)
        {
            var student = _context.Students.First(stud => stud.RoadMap.Id.Equals(id));
            student.RoadMap = null;
            _context.Update(student);
            
            var roadMap = await _context.RoadMaps
                .Include(map => map.Requirements)
                .ThenInclude(requirements => requirements.Competence)
                .Where(map => map.Id.Equals(id))
                .FirstAsync();

            if (roadMap == null)
            {
                return NotFound();
            }
            
            foreach (var roadMapRequirement in roadMap.Requirements)
            {
                _context.RoadMapAchievements.Remove(roadMapRequirement);
            }

            _context.RoadMaps.Remove(roadMap);
            await _context.SaveChangesAsync();

            return roadMap;
        }

        [Authorize("StudentOrStudentbegeleider")]
        private bool RoadMapExists(Guid id)
        {
            return _context.RoadMaps.Any(e => e.Id == id);
        }
    }
}
