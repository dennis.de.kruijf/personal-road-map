using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PersonalRoadMap.API.Services;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;

namespace PersonalRoadMap.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ModuleController : ControllerBase
    {
        private readonly IModuleService _Service;
        private readonly PersonalRoadMapDbContext _context;

        public ModuleController(PersonalRoadMapDbContext context, IModuleService service)
        {
            _Service = service;
            _context = context;
        }
       

        // GET: api/Module
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AcademicYear>>> GetAcademicYears()
        {
            return await _context.AcademicYears
                .Include(year => year.Semesters)
                .ThenInclude(semester => semester.Modules)
                .ThenInclude(semesterModule => semesterModule.Module)
                .ToListAsync();
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Module>>> GetModules()
        {
            
            return await _context.Modules
                .Include(req => req.Requirements)
                .Include(ach => ach.Achievements)
                .ToListAsync();
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<Module>> GetModule(Guid id)
        {
            var modules = await _context.Modules
                .Include(req => req.Requirements)
                .ThenInclude(req => req.Competence)
                .Include(ach => ach.Achievements)
                .ThenInclude(ach => ach.Competence)
                .Where(x => x.Id.Equals(id)).FirstAsync();
                      
            return modules;
        }

        // GET: api/Module/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Module>> GetModuleByIdAsync(Guid id)
        {
            var module = await _Service.GetModuleByIdAsync(id).ConfigureAwait(false);

            if (module == null)
            {
                return NotFound();
            }

            return Ok(module);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CompetanceModuleRequirement>>> GetModuleRequirements(Guid id)
        {
            var requirements = await _Service.GetModuleRequirementsByIdAsync(id).ConfigureAwait(false);

            if (requirements == null)
            {
                return NotFound();
            }

            return Ok(requirements);
        }

        // PUT: api/Module/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize("Opleidingsverantwoordelijke")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAcademicYear(Guid id, AcademicYear academicYear)
        {
            if (id != academicYear.Id)
            {
                return BadRequest();
            }

            _context.Entry(academicYear).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AcademicYearExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        [Authorize("Opleidingsverantwoordelijke")]
        [HttpPost("{id}")]
        public async Task<ActionResult<Guid>> DeleteModule(Guid id)
        {
            
                var requirements = await _context.ModuleRequirements.Where(x => x.ModuleId == id).ToListAsync();
                foreach (var req in requirements)
                {
                    _context.ModuleRequirements.Remove(req);
                }
                var module = await _context.Modules.Where(x => x.Id == id).FirstAsync();
                _context.Modules.Remove(module);

               await _context.SaveChangesAsync();

                return Ok();                      
        }

        // POST: api/Module
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize("Opleidingsverantwoordelijke")]
        [HttpPost]
        public async Task<ActionResult<AcademicYear>> PostAcademicYear(AcademicYear academicYear)
        {
            _context.AcademicYears.Add(academicYear);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAcademicYear", new { id = academicYear.Id }, academicYear);
        }
        //checks if a module is new or edited. if edited it removes the requirements and adds the new ones
        //loops through the competences to bind them using the database.
        //in the edit it deletes old competences then adds all the submited ones
        [Authorize("Opleidingsverantwoordelijke")]
        [HttpPost]
        public async Task<ActionResult<Module>> PostModule(Module module)
        {
          
            if (await _context.Modules.Where(i => i.Id == module.Id).AnyAsync())
            {               
                var oldreq = await _context.ModuleRequirements.Where(x => x.ModuleId == module.Id).ToListAsync();
                foreach(var req in oldreq)
                { 
                    _context.ModuleRequirements.Remove(req);
                }
                foreach (var req in module.Requirements)
                {
                    var requirement = new CompetanceModuleRequirement();
                    requirement.CompetenceId = req.CompetenceId;
                    requirement.ModuleId = module.Id;
                    _context.ModuleRequirements.Add(requirement);

                }
                _context.Entry(module).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }                    
            else
            {
                var newModule = new Module();
                newModule.Id = module.Id;
                newModule.Name = module.Name;
                newModule.Description = module.Name;
                newModule.EC = module.EC;               
                _context.Modules.Add(newModule);
                await _context.SaveChangesAsync();
                foreach (var req in module.Requirements)
                {
                    var requirement = new CompetanceModuleRequirement();
                    requirement.CompetenceId = req.CompetenceId;
                    requirement.ModuleId = module.Id;
                    _context.ModuleRequirements.Add(requirement);

                }
            }
           
            await _context.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/Module/5
        [Authorize("Opleidingsverantwoordelijke")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<AcademicYear>> DeleteAcademicYear(Guid id)
        {
            var academicYear = await _context.AcademicYears.FindAsync(id);
            if (academicYear == null)
            {
                return NotFound();
            }

            _context.AcademicYears.Remove(academicYear);
            await _context.SaveChangesAsync();

            return academicYear;
        }

        private bool AcademicYearExists(Guid id)
        {
            return _context.AcademicYears.Any(e => e.Id == id);
        }
    }
}
