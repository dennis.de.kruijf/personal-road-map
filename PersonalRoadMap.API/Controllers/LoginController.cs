﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;

namespace PersonalRoadMap.API.Controllers
{
    
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly PersonalRoadMapDbContext _context;

        public LoginController(PersonalRoadMapDbContext context)
        {
            _context = context;
        }

        // GET: api/Login
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Competence>>> Login()
        {
             return Ok("Logged in succesfully");
        }
        
        [Authorize]
        [HttpGet]
        public async Task<ActionResult> Test()
        {
            var tyhing = User.FindFirst(ClaimTypes.NameIdentifier);  
            
            return Ok("Logged in succesfully");
        }

    }

}
