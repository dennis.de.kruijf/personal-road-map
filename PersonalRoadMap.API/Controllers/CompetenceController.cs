using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;

namespace PersonalRoadMap.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompetenceController : ControllerBase
    {
        private readonly PersonalRoadMapDbContext _context;

        public CompetenceController(PersonalRoadMapDbContext context)
        {
            _context = context;
        }

        // GET: api/Competence
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Competence>>> GetCompetences()
        
        {
            return await _context.Competences.ToListAsync();
        }

        // GET: api/Competence/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Competence>> GetCompetence(Guid id)
        {
            var competence = await _context.Competences
                .Include(comp => comp.ModuleAchievements)
                .ThenInclude(moduleAchievements => moduleAchievements.Module)
                .Include(comp => comp.ModuleRequirements)
                .ThenInclude(moduleRequirements => moduleRequirements.Module)
                .FirstAsync();
            
            if (competence == null) {
                return NotFound();
            }

            return competence;    
        }

        // PUT: api/Competence/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize("Opleidingsverantwoordelijke")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompetence(Guid id, Competence competence)
        {
            if (id != competence.Id)
            {
                return BadRequest();
            }

            _context.Entry(competence).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompetenceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Competence
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize("Opleidingsverantwoordelijke")]
        [HttpPost]
        public async Task<ActionResult<Competence>> PostCompetence(Competence competence)
        {
            _context.Competences.Add(competence);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCompetence", new { id = competence.Id }, competence);
        }

        // DELETE: api/Competence/5
        [Authorize("Opleidingsverantwoordelijke")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Competence>> DeleteCompetence(Guid id)
        {
            var competence = await _context.Competences.FindAsync(id);
            if (competence == null)
            {
                return NotFound();
            }

            _context.Competences.Remove(competence);
            await _context.SaveChangesAsync();

            return competence;
        }

        private bool CompetenceExists(Guid id)
        {
            return _context.Competences.Any(e => e.Id == id);
        }
    }
}
