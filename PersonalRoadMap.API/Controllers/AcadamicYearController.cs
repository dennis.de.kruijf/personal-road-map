﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PersonalRoadMap.API.Services;
using PersonalRoadMap.Entities;

namespace PersonalRoadMap.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AcadamicYearController : ControllerBase
    {
        private readonly IAcadamicYearService _Service;

        public AcadamicYearController(IAcadamicYearService service)
        {
            _Service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<AcademicYear>>> GetAcademicYears()
        {
          var acadamicYears = await _Service.GetAcademicYearWithModulesAndSemestersAsync().ConfigureAwait(false);

            return Ok(acadamicYears);
        }

        // GET: api/AcadamicYear/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AcademicYear>> GetAcademicYear(Guid id)
        {
            var academicYear = await _Service.GetAcademicYearWithModulesAndSemestersByIdAsync(id);

            if (academicYear == null)
            {
                return NotFound();
            }

            return Ok(academicYear) ;
        }
    }
}