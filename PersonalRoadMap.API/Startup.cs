using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Net.Http.Headers;
using PersonalRoadMap.API.Services;
using PersonalRoadMap.Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using PersonalRoadMap.API.Services;
using PersonalRoadMap.API.Authorization;

namespace PersonalRoadMap.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IAcadamicYearService, AcadamicYearService>();
            services.AddScoped<IModuleService, ModuleService>();
            services.AddScoped<IRoadMapService, RoadMapService>();
            services.AddScoped<IStudentService, StudentService>();
            services.AddCors(options =>
            {
                options.AddPolicy(name: "_myAllowSpecificOrigins",
                    builder =>
                    {
                        builder.WithOrigins(
                                "https://localhost:6001",
                                "http://localhost:6001",
                                "https://localhost:6000",
                                "http://localhost:6000",
                                "https://localhost:5001",
                                "http://localhost:5001",
                                "https://localhost:5000",
                                "http://localhost:5000",
                                "https://localhost:63342", //Jasmine test port
                                "http://localhost:63342", //Jasmine test port
                                "https://localhost:3000",
                                "http://localhost:3000"
                            ).WithHeaders(HeaderNames.ContentType)
                                .AllowCredentials()
                                .AllowAnyHeader()
                                .AllowAnyMethod();
                    });
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII
                        .GetBytes(Configuration.GetSection("AzureAd.Secret").ToString())),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(StudentAuthorizationPolicy.Name, StudentAuthorizationPolicy.Build);
                options.AddPolicy(StudentbegeleiderAuthorizationPolicy.Name, StudentbegeleiderAuthorizationPolicy.Build);
                options.AddPolicy(OpleidingsverantwoordelijkeAuthorizationPolicy.Name, OpleidingsverantwoordelijkeAuthorizationPolicy.Build);
                options.AddPolicy(StudentOrStudentbegeleiderAuthorizationPolicy.Name, StudentOrStudentbegeleiderAuthorizationPolicy.Build);
            });

            services.AddControllers();
            services.AddDbContext<PersonalRoadMapDbContext>();
            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors("_myAllowSpecificOrigins");

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
