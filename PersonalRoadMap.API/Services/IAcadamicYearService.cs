﻿using PersonalRoadMap.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalRoadMap.API.Services
{
	public interface IAcadamicYearService
	{
		Task<IEnumerable<AcademicYear>> GetAcademicYearWithModulesAndSemestersAsync();
		Task<AcademicYear> GetAcademicYearWithModulesAndSemestersByIdAsync(Guid id);
	}
}
