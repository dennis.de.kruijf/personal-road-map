﻿using Microsoft.EntityFrameworkCore;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalRoadMap.API.Services
{
	public class ModuleService : IModuleService
	{
		public async Task<Module> GetModuleByIdAsync(Guid id)
		{
			using(var context =  new PersonalRoadMapDbContext())
			{
				var module = await context.Modules.Include(x => x.Requirements).ThenInclude(x => x.Competence).Include(x => x.Achievements).ThenInclude(x => x.Competence).FirstOrDefaultAsync(x => x.Id == id);

				return module;
			}
		}

		public async Task<IEnumerable<CompetanceModuleRequirement>> GetModuleRequirementsByIdAsync(Guid id)
		{
			using (var context = new PersonalRoadMapDbContext())
			{
				var requirements = await context.ModuleRequirements
				.Include(c => c.Competence)
				.Where(i => i.ModuleId == id)
				.ToListAsync();

				return requirements;
			}
		}
	}
}
