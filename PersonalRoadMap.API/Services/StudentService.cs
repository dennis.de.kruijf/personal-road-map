﻿using Microsoft.EntityFrameworkCore;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalRoadMap.API.Services
{
	public class StudentService : IStudentService
	{
		public async Task<Student> GetStudent(Guid id)
		{
			using(var context = new PersonalRoadMapDbContext())
			{
				var student = await context.Students.FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);

				return student;
			}
		}

		public async Task<IEnumerable<Student>> GetStudents()
		{
			using(var context = new PersonalRoadMapDbContext())
			{
				var students = await context.Students.ToListAsync();

				return students;
			}
		}
	}
}
