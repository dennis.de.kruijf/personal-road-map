﻿using PersonalRoadMap.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalRoadMap.API.Services
{
	public interface IModuleService
	{
		Task<Module> GetModuleByIdAsync(Guid id);
		Task<IEnumerable<CompetanceModuleRequirement>> GetModuleRequirementsByIdAsync(Guid id);
	}
}
