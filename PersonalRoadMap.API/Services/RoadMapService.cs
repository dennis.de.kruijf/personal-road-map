﻿using Microsoft.EntityFrameworkCore;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalRoadMap.API.Services
{
	public class RoadMapService : IRoadMapService
	{

		public async Task<RoadMap> GetByIdAsync(Guid id)
		{
			using (var context = new PersonalRoadMapDbContext())
			{
				var roadmap = await context.RoadMaps
					.Include(map => map.Requirements)
					.ThenInclude(requirements => requirements.Competence)
					.FirstOrDefaultAsync(x => x.Id ==  id).ConfigureAwait(false);

				return roadmap;
			}
		}

		private async  Task<bool> RoadMapExists(Guid id)
		{
			using (var context = new PersonalRoadMapDbContext())
			{
				return await context.RoadMaps.AnyAsync(e => e.Id == id).ConfigureAwait(false);
			}
		}

		public async Task<bool> UpdateAsync(Guid id, RoadMap roadMap)
		{
			using (var context = new PersonalRoadMapDbContext())
			{
				context.Entry(roadMap).State = EntityState.Modified;

			try
			{
				await context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!await RoadMapExists(id).ConfigureAwait(false))
				{
					return false;
				}
				else
				{
					throw;
				}
			}
				return true;
			}
		}

		public async Task<RoadMap> GetByStundentIdAsync(Guid id)
		{
			using (var context = new PersonalRoadMapDbContext())
			{
				var student = await context.Students.Include(x => x.RoadMap).ThenInclude(x => x.Modules).ThenInclude(x => x.Module).FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);

				if(student == null)
				{
					return null;
				}

				var roadmap = student.RoadMap;

				return roadmap;
			}
		}
	}
}
