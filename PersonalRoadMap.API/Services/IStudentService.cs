﻿using PersonalRoadMap.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalRoadMap.API.Services
{
	public interface IStudentService
	{
		Task<IEnumerable<Student>> GetStudents();
		Task<Student> GetStudent(Guid id);
	}
}
