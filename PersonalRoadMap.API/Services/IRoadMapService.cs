﻿using PersonalRoadMap.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalRoadMap.API.Services
{
	public interface IRoadMapService
	{
		Task<RoadMap> GetByIdAsync(Guid id);  
		Task<RoadMap> GetByStundentIdAsync(Guid id);  
	}
}
