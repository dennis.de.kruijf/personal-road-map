﻿using Microsoft.EntityFrameworkCore;
using PersonalRoadMap.Database;
using PersonalRoadMap.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalRoadMap.API.Services
{
	public class AcadamicYearService : IAcadamicYearService
	{

		public async Task<IEnumerable<AcademicYear>> GetAcademicYearWithModulesAndSemestersAsync()
		{
			using(var context = new PersonalRoadMapDbContext())
			{
				return await context.AcademicYears
				   .Include(year => year.Semesters)
				   .ThenInclude(semester => semester.Modules)
				   .ThenInclude(semesterModule => semesterModule.Module)
				   .ToListAsync().ConfigureAwait(false);
			}
		}

		public async Task<AcademicYear> GetAcademicYearWithModulesAndSemestersByIdAsync(Guid id)
		{
			using (var context = new PersonalRoadMapDbContext())
			{
				return await context.AcademicYears
				   .Include(year => year.Semesters)
				   .ThenInclude(semester => semester.Modules)
				   .ThenInclude(semesterModule => semesterModule.Module)
				   .FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);
			}
		}
	}
}
