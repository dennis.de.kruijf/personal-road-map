﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace PersonalRoadMap.Website.Controllers
{
    public class CompetenceController : BaseController
    {
        private static readonly HttpClient client = new HttpClient();
        private static string apiUrl = "https://localhost:5001";
        
        
       
        // GET: Competence
        public async Task<ActionResult> Index()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:5001/api/");
                //HTTP GET
                var responseTask = client.GetAsync("competence");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    var readTask = result.Content.ReadAsAsync<Entities.Competence[]>();
                    readTask.Wait();
                   
                    var competences = readTask.Result;              
                    return View("/views/BackOffice/competence/CompetenceOverview.cshtml",competences);

                }
                return View("/views/BackOffice/competence/CompetenceOverview.cshtml" );

            }        
        }

        // GET: Competence/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Competence/CreateCompetence
        public ActionResult CreateCompetence()
        {
            return View("/views/BackOffice/competence/CreateCompetence.cshtml");
        }

        // POST: Competence/CreateCompetence
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateCompetence([FromForm] Entities.Competence competence)
        {
            try
            {
                var values = new Dictionary<string, string>
                {
                        { "CompetenceName", competence.CompetenceName },
                        { "Level",  competence.Level.ToString()}
                };

                var content = new FormUrlEncodedContent(values);

                var response = await client.PostAsync("", content);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Competence/Edit/5
        public ActionResult Edit(int id)
        {
            return View("/views/BackOffice/competence/CreateCompetence.cshtml");
        }

        // POST: Competence/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Competence/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Competence/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}