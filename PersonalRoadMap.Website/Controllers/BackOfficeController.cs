﻿
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
namespace PersonalRoadMap.Website.Controllers
{
    public class BackOfficeController : Controller
    {
        public IActionResult Index()
        {
            
            return View("/views/BackOffice/Overview/Overview.cshtml" );
        }
    }
}
