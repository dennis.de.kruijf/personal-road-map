﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using PersonalRoadMap.Website.Models;

namespace PersonalRoadMap.Website.Controllers
{
	public class HomeController : BaseController
	{
		private readonly ILogger<HomeController> _logger;

		public HomeController(ILogger<HomeController> logger)
		{
			_logger = logger;
		}

		public IActionResult Index()
		{
			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[Authorize]
		[Route("/profile")]
		public IActionResult Login()
		{
			var identity = ((ClaimsIdentity)HttpContext.User.Identity);
			var name = identity.Claims.FirstOrDefault(c => c.Type == "name")?.Value;

			if (identity.IsAuthenticated)
			{
				AddNotificationMessage("Welkom " + name + "!");
			}
			return View("Index");
		}

		[Authorize]
		[Route("/token")]
		public IActionResult Token()
		{
			var identity = ((ClaimsIdentity)HttpContext.User.Identity);
			var token = "";

			if (identity.IsAuthenticated)
			{
				// Authentication was successful, so generate JWT.
				var tokenHandler = new JwtSecurityTokenHandler();
				var key = Encoding.ASCII.GetBytes(Startup.Configuration.GetSection("AzureAd.Secret").ToString());
				var tokenDescriptor = new SecurityTokenDescriptor
				{
					Subject = identity,
					Expires = DateTime.UtcNow.AddDays(7),
					SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
				};
				var token_description = tokenHandler.CreateToken(tokenDescriptor);
				token = tokenHandler.WriteToken(token_description);
			}
			return Ok(token);
		}

		[Route("/logout")]
		public async Task<IActionResult> Logout()
		{
			var identity = ((ClaimsIdentity)HttpContext.User.Identity);
			if (identity.IsAuthenticated) {
				await HttpContext.SignOutAsync();
				AddNotificationMessage("U bent successvol uitgelogd.");

				return View();
			}
			return RedirectToAction("Index");
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
