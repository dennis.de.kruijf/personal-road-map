﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PersonalRoadMap.Website.Models;
using System.Text.Json;
using Newtonsoft.Json;

namespace PersonalRoadMap.Website.Controllers
{
	public abstract class BaseController : Controller
	{
		private List<Notification> notificationMessages;

		public BaseController()
		{
			notificationMessages = new List<Notification>();

		}

		public void AddNotificationMessage(string message)
		{
			notificationMessages.Add(new Notification() { Message = message });
			ViewData["notificationMessages"] = JsonConvert.SerializeObject(notificationMessages);
		}
	}
}
