﻿//General includes
const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const order = require('gulp-order');

//CSS includes
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');

//JS Includes
const babel = require('gulp-babel');

function css(done) {
    gulp.src('src/css/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(concat('style.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./wwwroot/css/'))
        .pipe(browserSync.stream());
    done();
}

function jsMove(done, fileOrder, src, name) {
    gulp.src(src)
        .pipe(order(fileOrder))
        .pipe(concat(name))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest('./wwwroot/js/'));
    done();

}

function js(done) {
    const files_js_order = [];
    const src = 'src/js/**/*.js';
    const name = 'app.js';

    jsMove(done, files_js_order, src, name);
    done();
}

function vendor(done) {
    const files_js_order = ['vendor/jquery.js', 'vendor/popper.min.js', 'vendor/bootstrap.min.js'];
    const src = 'src/vendor/**/*.js';
    const name = 'vendor.js';

    jsMove(done, files_js_order, src, name);
    done();
}

function build(done) {
    css(done);
    js(done);
    vendor(done);
    done();
}


gulp.task('start', function (done) {
    browserSync.init({
        server: {
            baseDir: "./wwwroot"
        }
    });

    gulp.watch("src/css/*.scss").on('change', () => {
        css(done);
        browserSync.reload()
    });
    gulp.watch("wwwroot/*.html").on('change', () => {
        browserSync.reload()
    });
    gulp.watch("src/js/*.js").on('change', () => {
        js(done);
        browserSync.reload()
    });
});


exports.css = css;
exports.js = js;
exports.vendor = vendor;
exports.build = build;
