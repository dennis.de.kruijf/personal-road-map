"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ApiController = /*#__PURE__*/function () {
  function ApiController() {
    _classCallCheck(this, ApiController);

    this.destination = 'https://localhost:5001/api';
    this.bearerToken = null;
    this.refreshToken = null;
    this.baseCallBackUrl = "https://localhost:6001";
  }

  _createClass(ApiController, [{
    key: "get",
    value: function get(endpoint) {
      var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      // return $.get(`${this.destination}/${endpoint}/${id ? id : ""}`);
      return $.ajax({
        url: "".concat(this.destination, "/").concat(endpoint, "/").concat(id ? id : ""),
        type: 'GET',
        success: function success(data, status) {
          return data;
        },
        error: function error(html, status, _error) {
          return _error;
        },
        beforeSend: function beforeSend(xhr) {
          if (sessionStorage.getItem('token')) {
            console.info(sessionStorage.getItem('token'));
            xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token'));
          }
        }
      });
    }
  }, {
    key: "GetCalbackUrl",
    value: function GetCalbackUrl() {
      return this.baseCallBackUrl;
    }
  }, {
    key: "put",
    value: function put(endpoint, id, newData) {
      return $.ajax({
        url: "".concat(this.destination, "/").concat(endpoint, "/").concat(id),
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(newData),
        success: function success(data, status) {
          return true;
        },
        error: function error(html, status, _error2) {
          return _error2;
        },
        beforeSend: function beforeSend(xhr) {
          if (sessionStorage.getItem('token')) {
            xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token'));
          }
        }
      });
    }
  }, {
    key: "post",
    value: function post(endpoint, newData) {
      return $.ajax({
        url: "".concat(this.destination, "/").concat(endpoint),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(newData),
        success: function success(data, status) {
          return true;
        },
        error: function error(html, status, _error3) {
          return _error3;
        },
        beforeSend: function beforeSend(xhr) {
          if (sessionStorage.getItem('token')) {
            xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token'));
          }
        }
      });
    }
  }, {
    key: "delete",
    value: function _delete(endpoint, id) {
      $.ajax({
        url: "".concat(this.destination, "/").concat(endpoint, "/").concat(id),
        type: 'DELETE',
        success: function success(data, status) {
          return true;
        },
        error: function error(html, status, _error4) {
          return _error4;
        },
        beforeSend: function beforeSend(xhr) {
          if (sessionStorage.getItem('token')) {
            xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token'));
          }
        }
      });
    }
  }]);

  return ApiController;
}();

var EducationSelection = /*#__PURE__*/function () {
  function EducationSelection() {
    _classCallCheck(this, EducationSelection);

    this.roadmap = {};
    this.options = {};
    this.flatOptions = {};
    this.optionsContainerId = "";
    this.roadmapContainerId = "";
    this.saveButtonId = "";
  }

  _createClass(EducationSelection, [{
    key: "__init",
    value: function __init(optionsContainerId, roadmapContainerId, saveButtonId) {
      var _this = this;

      return new Promise(function (resolve, reject) {
        _this.API = new ApiController(); // Was module/GetAcademicYears

        _this.API.get('acadamicyear').then(function (data) {
          _this.options = data;
          _this.flatOptions = _this.flattenOptions(data);
          _this.optionsContainerId = optionsContainerId;

          _this.renderModules(data);

          _this.roadmapContainerId = roadmapContainerId;

          _this.renderRoadmap(data);

          _this.saveButtonId = saveButtonId;

          if ($("#" + _this.saveButtonId).length > 0) {
            $("#" + _this.saveButtonId).on('click', function () {
              return _this.handleSaveRoadmap();
            });
          }

          resolve();
        });
      });
    }
  }, {
    key: "mockData",
    value: function mockData() {
      var res = {};
      var id = 1;

      for (var i = 0; i < 4; i++) {
        var schooljaar = {};

        for (var x = 0; x < 2; x++) {
          var semester = [];

          for (var y = 0; y < 10; y++) {
            semester.push({
              id: id,
              'naam': "Module ".concat(y + 1, ", S").concat(x + 1, ", J").concat(i + 1),
              'EC': 30
            });
            id++;
          }

          schooljaar[x] = semester;
        }

        res[i + 1] = schooljaar;
      }

      return res;
    }
  }, {
    key: "flattenOptions",
    value: function flattenOptions(options) {
      var _ref2;

      var moduleData = options.map(function (y) {
        var _ref;

        var opt = y;
        var res = opt.semesters.map(function (s) {
          return s.modules.map(function (x) {
            return x.module;
          });
        });
        return (_ref = []).concat.apply(_ref, _toConsumableArray(res));
      });
      return (_ref2 = []).concat.apply(_ref2, _toConsumableArray(moduleData));
    }
  }, {
    key: "setRoadmap",
    value: function setRoadmap(year, semester, module) {
      if (!(year in this.roadmap)) {
        this.roadmap[year] = {};
      }

      this.roadmap[year][semester - 1] = module;
      this.renderRoadmap(this.options);
    }
  }, {
    key: "selectModule",
    value: function selectModule(e) {
      //Get the object and the data id;
      var module = $(e.target).closest('.module');
      var moduleId = module.attr('data-module'); //Check if selected module is valid;

      if (moduleId == null) {
        console.error("Invalid module clicked");
        return;
      } //Find the list.


      var card = module.closest('.card');
      var year = card.attr('data-year');
      var semester = card.attr('data-semester');
      var selectModule = this;
      var moduleObj = this.flatOptions.filter(function (m) {
        return m.id === moduleId;
      });

      if (moduleObj.length > 0) {
        moduleObj = moduleObj[0];
        var controller = new ApiController();
        controller.get('module/GetModule', moduleObj.id).then(function (resultModule) {
          var validateModel = Validate(resultModule);

          if (!validateModel.status) {
            ErrorMessage(validateModel.errors);
          } else {
            selectModule.setRoadmap(year, semester, moduleObj); //Remove selected state from current

            card.find('.module-list .module.selected').removeClass('selected'); //Set selected state to current

            module.addClass('selected'); //Enable next card

            card.next().removeClass('disabled');
          }
        });
      } else {
        console.error("Invalid module clicked");
      }
    }
  }, {
    key: "renderModuleButton",
    value: function renderModuleButton(moduleData) {
      var _moduleData$name,
          _moduleData$ec,
          _this2 = this;

      moduleData = 'module' in moduleData ? moduleData.module : moduleData;
      var module = document.createElement('div');
      module.classList.add('module');
      module.setAttribute('data-module', moduleData.id);
      var content = document.createElement('div');
      content.classList.add('content');
      var title = document.createElement('h3');
      title.innerText = (_moduleData$name = moduleData.name) !== null && _moduleData$name !== void 0 ? _moduleData$name : "???";
      var ec = document.createElement('span');
      ec.classList.add('ec');
      ec.innerText = (_moduleData$ec = moduleData.ec) !== null && _moduleData$ec !== void 0 ? _moduleData$ec : 0;
      content.append(title, ec);
      var info = document.createElement('div');
      info.setAttribute('class', 'tooltip');
      var infoimage = document.createElement('img');
      infoimage.setAttribute('src', 'https://image.flaticon.com/icons/svg/66/66788.svg');
      infoimage.setAttribute('class', 'infoimage');
      infoimage.setAttribute('id', 'infoimage');
      var infotext = document.createElement('span');
      infotext.innerText = "Competenties:";
      infoimage.addEventListener("mouseover", function () {
        new ApiController().get('module/GetModuleRequirements/' + moduleData.id).then(function (data) {
          data.forEach(function (item) {
            infotext.innerHTML += " " + item['competence']['competenceName'] + ', </br>';
          });
        });
      }, {
        once: true
      });
      infotext.setAttribute('class', 'tooltiptext');
      infotext.setAttribute('id', 'tooltiptext');
      info.append(infotext);
      info.append(infoimage);
      module.append(content, info);
      module.addEventListener("click", function (e) {
        return _this2.selectModule(e);
      });
      return module;
    }
  }, {
    key: "renderSemesterCard",
    value: function renderSemesterCard(year, semester, modules, children) {
      var _this3 = this;

      var card = document.createElement('section');
      card.classList.add('card');

      if (!(children === 0)) {
        card.classList.add('disabled');
      }

      card.setAttribute("data-year", year);
      card.setAttribute("data-semester", semester);
      var h2 = document.createElement('h2');
      h2.innerText = "Jaar ".concat(year, ", Semester ").concat(semester);
      var list = document.createElement('div');
      list.classList.add('module-list');
      card.append(h2, list);
      var options = modules.map(function (m) {
        return _this3.renderModuleButton(m);
      });
      list.append.apply(list, _toConsumableArray(options));
      return card;
    }
  }, {
    key: "renderRoadmapSegment",
    value: function renderRoadmapSegment(year, semesters) {
      var _this4 = this;

      semesters = semesters.semesters;
      var segment = document.createElement('div');
      segment.classList.add('semester');
      var tag = document.createElement('span');
      tag.classList.add('tag');
      tag.innerText = "Jaar ".concat(year);
      segment.append(tag);
      var sNo = 0;
      var modules = semesters.map(function (s) {
        if (year in _this4.roadmap && sNo in _this4.roadmap[year] && _this4.roadmap[year][sNo] !== null) {
          sNo++;
          return _this4.renderModuleButton(_this4.roadmap[year][sNo - 1]);
        } else {
          sNo++;
          return _this4.renderModuleButton({});
        }
      });
      segment.append.apply(segment, _toConsumableArray(modules));
      return segment;
    }
  }, {
    key: "renderModules",
    value: function renderModules(options) {
      var _this5 = this;

      var element = document.getElementById(this.optionsContainerId);

      if (element != null) {
        element.innerHTML = "";
        var cards = [];
        options.forEach(function (year) {
          var semesterNo = 1;
          year.semesters.forEach(function (semester) {
            cards.push(_this5.renderSemesterCard(parseInt(year.year, 10), semesterNo, semester.modules, cards.length));
            semesterNo++;
          });
        });
        element.append.apply(element, cards);
      } else {
        console.error("Could not append modules to element.");
      }
    }
  }, {
    key: "renderRoadmap",
    value: function renderRoadmap(options) {
      var _this6 = this;

      var element = document.getElementById(this.roadmapContainerId);

      if (element != null) {
        element.innerHTML = "";
        var segments = [];
        options.forEach(function (year) {
          segments.push(_this6.renderRoadmapSegment(year.year, year));
        });
        element.append.apply(element, segments);
      } else {
        console.error("Could not append roadmap to element.");
      }
    }
  }, {
    key: "flattenRoadMap",
    value: function flattenRoadMap(roadmap) {
      var _ref4;

      var rm = Object.keys(roadmap).map(function (r) {
        var _ref3;

        var res = Object.keys(roadmap[r]).map(function (y) {
          return roadmap[r][y].id;
        });
        return (_ref3 = []).concat.apply(_ref3, _toConsumableArray(res));
      });
      return (_ref4 = []).concat.apply(_ref4, _toConsumableArray(rm));
    }
  }, {
    key: "handleSaveRoadmap",
    value: function handleSaveRoadmap() {
      var _this7 = this;

      //change for update
      var API = new ApiController();
      API.get('Student').then(function (me) {
        var user = me;

        var RoadMapObject = _this7.flattenRoadMap(_this7.roadmap);

        console.log("ME", user);

        if (user.roadMapId == null) {
          // Should be impossible
          API.post('RoadMap', RoadMapObject).then(function (roadmap) {
            if (roadmap.id != null) {
              user.roadmapid = roadmap.id;
              API.put('Student', user.id, user);
            }
          });
        } else {
          API.put('RoadMap', user.roadMap.id, RoadMapObject).then(function (rm) {
            console.log("PUT", rm);
          });
        }
      });
    }
  }]);

  return EducationSelection;
}();

$(document).ready(function () {
  // make the request to the token endpoint
  var tokenUrl = "https://localhost:6001/token";
  $.get(tokenUrl).done(function (data) {
    sessionStorage.setItem('token', data);
  }).then(function () {
    var service = new StudentService();
  });
});

var ModuleController = /*#__PURE__*/function () {
  function ModuleController() {
    _classCallCheck(this, ModuleController);
  }

  _createClass(ModuleController, [{
    key: "__init",
    //renders the module overview, gets the competences for the edit/create screen,
    //binds an onclick to the submit button of the edit / create form
    //and lastly checks if the user is editing a module.
    value: function __init() {
      var _this8 = this;

      this.API = new ApiController();
      this.API.get('module/GetModules').then(function (data) {
        _this8.renderModulesoverview(data);

        _this8.getCompetences();

        document.getElementById("submitButton").onclick = _this8.postModule;
        var params = new URLSearchParams(location.search);
        console.log();

        if (params.get('id') != null) {
          _this8.editModule(params.get('id'));
        }
      });
    } //creates new module card and binds the delete/edit urls to the buttons

  }, {
    key: "renderModulesoverview",
    value: function renderModulesoverview(data) {
      var overview = document.getElementById('ModuleOverviewCards');

      if (overview != null) {
        data.forEach(function (item) {
          var module = document.createElement('div');
          module.setAttribute('class', "OverviewCard");
          var moduleLabel = document.createElement('label');
          moduleLabel.innerText = item.name;
          var br = document.createElement("br");
          var edit = document.createElement('img');
          edit.setAttribute('id', "editbutton");

          edit.onclick = function () {
            location.href = "module/edit/?id=" + item.id;
          };

          edit.setAttribute('src', 'http://icons.iconarchive.com/icons/icons8/windows-8/256/Editing-Edit-icon.png');
          var deletebutton = document.createElement('img');
          deletebutton.setAttribute('id', "deletebutton");
          deletebutton.setAttribute('src', 'https://image.flaticon.com/icons/svg/1345/1345874.svg');
          deletebutton.setAttribute('href', "module/edit/?id=" + item.id);

          deletebutton.onclick = function () {
            var alert = confirm("weet u zeker dat u de module wilt verwijderen?");

            if (alert == true) {
              var API = new ApiController();
              API.post("Module/DeleteModule/" + item.id);
              location.reload();
            } else {}
          };

          module.append(moduleLabel, br, edit, deletebutton);
          overview.append(module);
        });
      } else {
        console.error("Could not append modules to element.");
      }
    }
  }, {
    key: "editModule",
    //gets the existing module data if the user is editing a module. also loops through the competences and makes sure the selected ones are highlighted
    value: function editModule(moduleId) {
      var API = new ApiController();
      API.get("Module/GetModule/" + moduleId).then(function (data) {
        console.log(data);
        document.getElementById('Name').value = data['name'];
        document.getElementById('Description').value = data['description'];
        document.getElementById('EC').value = data['ec'];
        document.getElementById('id').value = data['id'];
        $(data["requirements"]).each(function (item) {
          var options = document.getElementById("CompetencesList").options;
          $(options).each(function (option) {
            if (this.value == data["requirements"][item]['competenceId']) {
              this.setAttribute('selected', 'selected');
            }
          });
        });
      });
    } //posts the form data to the api. using the apicontroller. this is both used when editing or creating a module.
    //because we arent using the asp.net modals for posting the submited data
    //is manupilated into the same json as asp.net would submit.
    //this way the api accepts the data

  }, {
    key: "postModule",
    value: function postModule() {
      if ($("#ModuleForm")[0].checkValidity()) {
        var API = new ApiController();
        var result = {};
        var CompetanceModuleRequirement = [];
        $.each($('#ModuleForm').serializeArray(), function () {
          if (this.name == "Competences") {
            CompetanceModuleRequirement.push({
              "competenceId": this.value,
              "moduleId": this['id']
            });
          } else {
            result[this.name] = this.value;
          }
        });
        result["requirements"] = CompetanceModuleRequirement;
        var result = API.post("Module/PostModule", result).fail(function (jqXHR, textStatus, error) {
          if (jqXHR.status == 401) {
            $('#errorDiv').append('<p class="error">Je hebt geen toegang</p>');
          }
        });
        window.location.href = "/module";
      } else {
        $("#ModuleForm")[0].reportValidity();
      }
    }
  }, {
    key: "getCompetences",
    //gets competences to fill the select box in the module form.
    value: function getCompetences() {
      var API = new ApiController();
      API.get("Competence").then(function (data) {
        data.forEach(function (item) {
          var option = document.createElement('option');
          option.setAttribute('value', item.id);
          option.innerText = item.competenceName;
          $('#CompetencesList').append(option);
        });
      });
    }
  }]);

  return ModuleController;
}(); // request permission on page load


document.addEventListener('DOMContentLoaded', function () {
  if (typeof NotificationsList !== 'undefined') {
    if (Notification.permission !== 'granted') {
      Notification.requestPermission();
    }

    for (var notification in NotificationsList) {
      notification = NotificationsList[notification];
      notify(notification.Message);
    }
  }
});

function notify(message) {
  if (typeof Notification !== 'undefined' && Notification.permission === 'granted') {
    var notification = new Notification('Bericht van Personal Road Map - Windesheim HBO ICT', {
      icon: window.location.origin + '/favicon.ico',
      body: message
    });

    notification.onclick = function () {
      window.open(window.location.href);
    };
  }

  var alert = $('<div class="alert alert-success alert-dismissable">' + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>');
  alert.appendTo("#alerts");
  alert.slideDown("slow").delay(3000).fadeOut(2000, function () {
    $(this).remove();
  });
}

var StudentService = function StudentService() {
  _classCallCheck(this, StudentService);

  var controller = new ApiController();
  $('#errorDiv').html('');
  controller.get("student/all").then(function (students) {
    var container = $('<div/>', {
      "class": 'student-container'
    });

    var _loop = function _loop(i) {
      parent = $('<div/>', {
        "class": 'student-card'
      });
      $('<h4><b>' + students[i].name + '</b></h4>').appendTo(parent);
      paraTag = $('<p>');
      $('<p class="student-title">SE</p>').appendTo(paraTag);
      $('<button/>', {
        text: 'Bekijk',
        click: function click() {
          localStorage.setItem('student', JSON.stringify(students[i]));
          GetRoadMapByStudentId();
        },
        "class": 'student-button'
      }).appendTo(paraTag);
      paraTag.appendTo(parent);
      parent.appendTo(container);
    };

    for (var i = 0; i < students.length; i++) {
      var parent;
      var paraTag;

      _loop(i);
    }

    var studentElement = $('#studentList');
    studentElement.append(container);
  }).fail(function (jqXHR, textStatus, error) {
    if (jqXHR.status == 401) {
      $('#errorDiv').append('<p class="error">Je hebt geen toegang</p>');
    }
  });
};

function GetRoadMapByStudentId() {
  var controller = new ApiController();
  var student = JSON.parse(localStorage.getItem('student'));
  controller.get('roadmap/GetByStudentId/' + student.id).then(function (roadmap) {
    $('#studentList').empty();
    var returnDiv = $('<div>', {
      "class": 'container'
    });
    var roadmapSection = $('<section/>');
    $('<h4>' + student.name + '<h4/>').appendTo(roadmapSection);
    var listDiv = $('<div/>', {
      "class": 'module-list'
    });

    for (var x = 0; x < roadmap.modules.length; x++) {
      var mapElement = roadmap.modules[x].module;
      var moduleDiv = $('<div/>', {
        "class": 'roadmap-module'
      });
      $('<div class="content"><h3>' + mapElement.name + '</h3><span class="ec">' + mapElement.ec + '</span> </div').appendTo(moduleDiv);
      moduleDiv.appendTo(listDiv);
    }

    listDiv.appendTo(roadmapSection);
    $('<button>', {
      text: "ga terug",
      click: function click() {
        var callbackUrl = controller.GetCalbackUrl();
        window.location.assign(callbackUrl + "/student");
      }
    }).appendTo(returnDiv);
    $('#Roadmap').html(roadmapSection);
    $('#Roadmap').append(returnDiv);
  }).fail(function (jqXHR, textStatus, error) {
    if (jqXHR.status == 401) {
      $('#errorDiv').append('<p class="error">Je hebt geen toegang</p>');
    }
  });
  ;
}

var returnModel = {
  status: false,
  errors: null
};

function CreateErrorReturnModel(requirements) {
  var errorList = [];
  var str = "Voor deze module dien je de volgende competentie(s) te hebben: ";

  for (var i = 0; i < requirements.length; i++) {
    if (requirements.length == 1) {
      str += requirements[i].CompetenceName + " op niveau " + requirements[i].Level;
    } else if (i == requirements.length - 1) {
      str += requirements[i].CompetenceName + " op niveau " + requirements[i].Level;
    } else {
      str += requirements[i].CompetenceName + " op niveau " + requirements[i].Level + ", ";
    }
  }

  errorList.push({
    message: str
  });
  returnModel.errors = errorList;
  returnModel.status = false;
  return returnModel;
}

function CreateSuccessReturnModel() {
  returnModel.status = true;
  return returnModel;
}

function ErrorMessage(errors) {
  $("#errorDiv").html("");

  for (var i = 0; i < errors.length; i++) {
    $("#errorDiv").append('<p class="error">' + errors[i].message + '</p>');
  }
}

function Validate(module) {
  var LocalStorage = window.localStorage;

  if (!module.requirements.length) {
    LocalStorage.setItem('achievements', JSON.stringify(module.achievements));
    return CreateSuccessReturnModel();
  }

  var currentAchievements = LocalStorage.getItem('achievements');

  if (currentAchievements == null) {
    var missingCompetences = [];

    for (var x = 0; x < module.requirements.length; x++) {
      missingCompetences.push({
        CompetenceName: module.requirements[x].competence.competenceName,
        Level: module.requirements[x].competence.level
      });
    }

    return CreateErrorReturnModel(missingCompetences);
  }

  var currentAchievementsJson = JSON.parse(currentAchievements);
  var competenceList = [];

  for (var i = 0; i < module.requirements.length; i++) {
    for (var x = 0; x < currentAchievementsJson.length; x++) {
      if (currentAchievementsJson[x].competence.competenceName == module.requirements[i].competence.competenceName && currentAchievementsJson[x].competence.level >= module.requirements[i].competence.level) {
        competenceList.push({
          CompetenceName: module.requirements[i].competence.competenceName
        });
      }
    }
  }

  var requirementCount = module.requirements.length;
  var competenceCount = competenceList.length;

  if (competenceList != null) {
    if (requirementCount == competenceCount) {
      LocalStorage.setItem('achievements', JSON.stringify(module.achievements));
      return CreateSuccessReturnModel();
    } else {
      var _missingCompetences = [];

      if (competenceList.length != 0) {
        for (var i = 0; i < competenceList.length; i++) {
          for (var x = 0; x < module.requirements.length; x++) {
            if (module.requirements[x].competence.competenceName != competenceList[i].CompetenceName) {
              _missingCompetences.push({
                CompetenceName: module.requirements[x].competence.competenceName,
                Level: module.requirements[x].competence.level
              });
            }
          }
        }

        return CreateErrorReturnModel(_missingCompetences);
      } else {
        for (var i = 0; i < module.requirements.length; i++) {
          _missingCompetences.push({
            CompetenceName: module.requirements[i].competence.competenceName,
            Level: module.requirements[i].competence.level
          });
        }

        return CreateErrorReturnModel(_missingCompetences);
      }
    }
  } else {
    return CreateErrorReturnModel(module.requirements);
  }
}