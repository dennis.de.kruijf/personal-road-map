﻿
let returnModel = { status: false, errors: null };

function CreateErrorReturnModel(requirements)
{
	let errorList = [];
	let str = "Voor deze module dien je de volgende competentie(s) te hebben: ";

	for (var i = 0; i < requirements.length; i++)
	{
		if (requirements.length == 1) {

			str += requirements[i].CompetenceName + " op niveau " + requirements[i].Level; 
		}
		else if (i == requirements.length -1)
		{
			str += requirements[i].CompetenceName + " op niveau " + requirements[i].Level;
		}
		else
		{
			str += requirements[i].CompetenceName + " op niveau " + requirements[i].Level + ", "; 
		}
	
		
	}

	errorList.push({ message: str });
	
	returnModel.errors = errorList;
	returnModel.status = false;

	return returnModel;
}

function CreateSuccessReturnModel()
{
	returnModel.status = true;

	return returnModel;
}

function ErrorMessage(errors)
{
	$("#errorDiv").html("");

	for (var i = 0; i < errors.length; i++) {
		$("#errorDiv").append('<p class="error">' + errors[i].message + '</p>');
	}
}


function Validate(module)
{

	let LocalStorage = window.localStorage;

	if (!module.requirements.length) {


		LocalStorage.setItem('achievements', JSON.stringify(module.achievements));

		return CreateSuccessReturnModel();
	}



	let currentAchievements = LocalStorage.getItem('achievements');



	if (currentAchievements == null) {
		let missingCompetences = [];
		for (var x = 0; x < module.requirements.length; x++) {

			missingCompetences.push({ CompetenceName: module.requirements[x].competence.competenceName, Level: module.requirements[x].competence.level });

		}
		return CreateErrorReturnModel(missingCompetences);
	}

	let currentAchievementsJson = JSON.parse(currentAchievements);
	let competenceList = [];

	for (var i = 0; i < module.requirements.length; i++) {
		for (var x = 0; x < currentAchievementsJson.length; x++) {
			if (currentAchievementsJson[x].competence.competenceName == module.requirements[i].competence.competenceName && currentAchievementsJson[x].competence.level >= module.requirements[i].competence.level) {
				competenceList.push({ CompetenceName: module.requirements[i].competence.competenceName });
			}

		}

	}

	let requirementCount = module.requirements.length;
	let competenceCount = competenceList.length;

	if (competenceList != null) {
		if (requirementCount == competenceCount)
		{
			LocalStorage.setItem('achievements', JSON.stringify(module.achievements));

			return CreateSuccessReturnModel();
		}
		else {
			let missingCompetences = [];

			if (competenceList.length != 0) {
				for (var i = 0; i < competenceList.length; i++) {
					for (var x = 0; x < module.requirements.length; x++) {
						if (module.requirements[x].competence.competenceName != competenceList[i].CompetenceName) {
							missingCompetences.push({ CompetenceName: module.requirements[x].competence.competenceName, Level: module.requirements[x].competence.level });
						}
					}
				}

				return CreateErrorReturnModel(missingCompetences);
			}
			else {
				for (var i = 0; i < module.requirements.length; i++) {
					missingCompetences.push({ CompetenceName: module.requirements[i].competence.competenceName, Level: module.requirements[i].competence.level });
				}

				return CreateErrorReturnModel(missingCompetences);
			}
		}
	}
	else {
		return CreateErrorReturnModel(module.requirements);
	}
}