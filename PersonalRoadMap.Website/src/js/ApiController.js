class ApiController {
    constructor() {
        this.destination = 'https://localhost:5001/api';
        this.bearerToken = null;
        this.refreshToken = null;
        this.baseCallBackUrl = "https://localhost:6001";
    };
    
    get(endpoint, id = null) {              
        // return $.get(`${this.destination}/${endpoint}/${id ? id : ""}`);

        return $.ajax({
            url: `${this.destination}/${endpoint}/${id ? id : ""}`,
            type: 'GET',
            success: function (data, status) {
                return data;
            },
            error: function (html, status, error) {
                return error;
            },
            beforeSend: function (xhr) {
                if (sessionStorage.getItem('token')) {
                    console.info(sessionStorage.getItem('token'));
                    xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token'));
                }
            },
        })
    }

    GetCalbackUrl() {
        return this.baseCallBackUrl;
    }

    put(endpoint, id, newData) {
        return $.ajax({
            url: `${this.destination}/${endpoint}/${id}`,
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(newData),
            success: function (data, status) {
                return true;
            },
            error: function (html, status, error) {
                return error;
            },
            beforeSend: function (xhr) {
                if (sessionStorage.getItem('token')) {
                    xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token'));
                }
            },
        })
    }
    
    post(endpoint, newData) {
        return $.ajax({
            url: `${this.destination}/${endpoint}`,
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(newData),
            success: function (data, status) {
                return true;
            },
            error: function (html, status, error) {
                return error;
            },
            beforeSend: function (xhr) {
                if (sessionStorage.getItem('token')) {
                    xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token'));
                }
            },
        })     
    }
    
    delete(endpoint, id) {
        $.ajax({
            url: `${this.destination}/${endpoint}/${id}`,
            type: 'DELETE',
            success: function (data, status) {
                return true;
            },
            error: function (html, status, error) {
                return error;
            },
            beforeSend: function (xhr) {
                if (sessionStorage.getItem('token')) {
                    xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token'));
                }
            },
        })
    }
}