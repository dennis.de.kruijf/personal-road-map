﻿
class StudentService {

constructor() {
	let controller = new ApiController();
	$('#errorDiv').html('');

	controller.get("student/all").then(function (students) {
		var container = $('<div/>', {
			class: 'student-container'
		});
			

		for (let i = 0; i < students.length; i++) {

			var parent = $('<div/>',
				{
					class: 'student-card',
				});

			$('<h4><b>' + students[i].name + '</b></h4>').appendTo(parent);


			var paraTag = $('<p>');


			$('<p class="student-title">SE</p>').appendTo(paraTag);

			$('<button/>', {
				text: 'Bekijk',
				click: function () {
					localStorage.setItem('student', JSON.stringify(students[i]));
					GetRoadMapByStudentId();

				},
				class: 'student-button',
			}).appendTo(paraTag);

			paraTag.appendTo(parent);

			parent.appendTo(container);
		}

		var studentElement = $('#studentList');

		studentElement.append(container);

	}).fail(function (jqXHR, textStatus, error)
	{
		if (jqXHR.status == 401) {
			$('#errorDiv').append('<p class="error">Je hebt geen toegang</p>');
		}
	});
}

	
}

function GetRoadMapByStudentId() {
	let controller = new ApiController();

	var student = JSON.parse(localStorage.getItem('student'));

	controller.get('roadmap/GetByStudentId/' + student.id)
		.then(function (roadmap) {

			$('#studentList').empty();

			var returnDiv = $('<div>', {
				class: 'container',
			});
			var roadmapSection = $('<section/>');

			
			$('<h4>' + student.name + '<h4/>').appendTo(roadmapSection);

			var listDiv = $('<div/>', {
				class: 'module-list', 
			});

			for (var x = 0; x < roadmap.modules.length; x++) {
				var mapElement = roadmap.modules[x].module;	

				var moduleDiv = $('<div/>',
					{
						class:'roadmap-module',
					});
				$('<div class="content"><h3>' + mapElement.name + '</h3><span class="ec">' + mapElement.ec + '</span> </div').appendTo(moduleDiv);

				moduleDiv.appendTo(listDiv);
			}
			listDiv.appendTo(roadmapSection);

			$('<button>', {
				text: "ga terug",
				click: function () {
					var callbackUrl = controller.GetCalbackUrl();
					window.location.assign(callbackUrl + "/student");
				},
			}).appendTo(returnDiv);

			$('#Roadmap').html(roadmapSection);
			$('#Roadmap').append(returnDiv);
		})
		.fail(function (jqXHR, textStatus, error) {

			if (jqXHR.status == 401) {
				$('#errorDiv').append('<p class="error">Je hebt geen toegang</p>');
			}
		});;
}
 