﻿class ModuleController {
    //renders the module overview, gets the competences for the edit/create screen,
    //binds an onclick to the submit button of the edit / create form
    //and lastly checks if the user is editing a module.
    __init() {
      
        this.API = new ApiController();
        this.API.get('module/GetModules').then(
            (data) => {    
                
                this.renderModulesoverview(data);
                this.getCompetences();
                
                document.getElementById("submitButton").onclick = this.postModule;
                let params = new URLSearchParams(location.search);
                console.log();
                if (params.get('id') != null) {
                    this.editModule(params.get('id'));
                }
            }
        );
    }   
    //creates new module card and binds the delete/edit urls to the buttons
    renderModulesoverview(data) {
        let overview = document.getElementById('ModuleOverviewCards');
        
        if (overview != null) {
            data.forEach(function (item) {
                let module = document.createElement('div');
                module.setAttribute('class', "OverviewCard")
                let moduleLabel = document.createElement('label');
                moduleLabel.innerText = item.name
                let br = document.createElement("br");
                let edit = document.createElement('img');
                edit.setAttribute('id', "editbutton");
            
                edit.onclick = function () {
                    location.href = "module/edit/?id=" + item.id
                };
                edit.setAttribute('src', 'http://icons.iconarchive.com/icons/icons8/windows-8/256/Editing-Edit-icon.png');
            
                let deletebutton = document.createElement('img');
             
                deletebutton.setAttribute('id', "deletebutton");
                deletebutton.setAttribute('src', 'https://image.flaticon.com/icons/svg/1345/1345874.svg');
                deletebutton.setAttribute('href', "module/edit/?id=" + item.id)
                deletebutton.onclick = function () {
                    var alert = confirm("weet u zeker dat u de module wilt verwijderen?");
                    if (alert == true) {
                        var API = new ApiController();
                        API.post("Module/DeleteModule/" + item.id);
                        location.reload();
                    } else {
                  
                    }
                };

                module.append(moduleLabel, br,  edit, deletebutton);
                overview.append(module);            
            })}
                else {
                    console.error("Could not append modules to element.");
                }  
    };

  
    //gets the existing module data if the user is editing a module. also loops through the competences and makes sure the selected ones are highlighted
    editModule(moduleId) {
        var API = new ApiController();
        API.get("Module/GetModule/" + moduleId).then(
            (data) => {
                console.log(data);
                document.getElementById('Name').value = data['name'];
                document.getElementById('Description').value = data['description'];
                document.getElementById('EC').value = data['ec'];
                document.getElementById('id').value = data['id'];
                
                $(data["requirements"]).each(function (item) {    
                 
                    var options = document.getElementById("CompetencesList").options;
                    $(options).each(function (option) {
                        if (this.value == data["requirements"][item]['competenceId']) {
                            this.setAttribute('selected' , 'selected')
                        }
                     
                    });
                });
            });
    }

    //posts the form data to the api. using the apicontroller. this is both used when editing or creating a module.
    //because we arent using the asp.net modals for posting the submited data
    //is manupilated into the same json as asp.net would submit.
    //this way the api accepts the data
    postModule() {  
        if ($("#ModuleForm")[0].checkValidity()) {
            var API = new ApiController();
            var result = {};
            var CompetanceModuleRequirement = []
            $.each($('#ModuleForm').serializeArray(), function () {
                if (this.name == "Competences") {

                    CompetanceModuleRequirement.push({ "competenceId": this.value, "moduleId": this['id'] });
                } else {
                    result[this.name] = this.value;
                }
            });
            result["requirements"] = CompetanceModuleRequirement;
            
            var result = API.post("Module/PostModule", result).fail(function (jqXHR, textStatus, error) {
                if (jqXHR.status == 401) {
                    $('#errorDiv').append('<p class="error">Je hebt geen toegang</p>');
                }
            });
        window.location.href = "/module";
        }
        else {
            $("#ModuleForm")[0].reportValidity();
        }
        
    };

    //gets competences to fill the select box in the module form.
    getCompetences() {
    var API = new ApiController();
    API.get("Competence").then(
        (data) => {
            data.forEach(function (item) {

                let option = document.createElement('option');
                option.setAttribute('value', item.id)
                option.innerText = item.competenceName;
                $('#CompetencesList').append(option);
            });
        });
}

}