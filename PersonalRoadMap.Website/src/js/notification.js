﻿// request permission on page load
document.addEventListener('DOMContentLoaded', function () {

    if (typeof NotificationsList !== 'undefined') {
        if (Notification.permission !== 'granted') {
            Notification.requestPermission();
        }

        for (var notification in NotificationsList) {
            notification = NotificationsList[notification];
            notify(notification.Message);
        }
    }
});

function notify(message) {

    if (typeof Notification !== 'undefined' && Notification.permission === 'granted') {
        var notification = new Notification('Bericht van Personal Road Map - Windesheim HBO ICT', {
            icon: window.location.origin + '/favicon.ico',
            body: message,
        });
        notification.onclick = function () {
            window.open(window.location.href);
        };
    }
    var alert =
        $('<div class="alert alert-success alert-dismissable">' +
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
            message + '</div>');
    alert.appendTo("#alerts");
    alert.slideDown("slow").delay(3000).fadeOut(2000, function () {
        $(this).remove();
    });
}
