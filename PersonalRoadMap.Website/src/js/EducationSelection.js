class EducationSelection {
    constructor() {
        this.roadmap = {};
        this.options = {};
        this.flatOptions = {};
        this.optionsContainerId = "";
        this.roadmapContainerId = "";
        this.saveButtonId = "";
    }

    __init(optionsContainerId, roadmapContainerId, saveButtonId) {
        return new Promise((resolve, reject) => {
            this.API = new ApiController();
            // Was module/GetAcademicYears
            this.API.get('acadamicyear').then(
                (data) => {
                    this.options = data;
                    this.flatOptions = this.flattenOptions(data);
                    this.optionsContainerId = optionsContainerId;
                    this.renderModules(data);

                    this.roadmapContainerId = roadmapContainerId;
                    this.renderRoadmap(data);

                    this.saveButtonId = saveButtonId;
                    if($("#" + this.saveButtonId).length > 0) {
                        $("#" + this.saveButtonId).on('click', () => this.handleSaveRoadmap());
                    }
                    
                    resolve();
                }
            );
        });
    }

    mockData() {
        let res = {};
        let id = 1;
        for (var i = 0; i < 4; i++) {
            let schooljaar = {};
            for (var x = 0; x < 2; x++) {
                let semester = [];
                for (var y = 0; y < 10; y++) {
                    semester.push({
                        id,
                        'naam': `Module ${y + 1}, S${x + 1}, J${i + 1}`,
                        'EC': 30
                    });
                    id++;
                }
                schooljaar[x] = semester;
            }
            res[i + 1] = schooljaar;
        }
        return res;
    }

    flattenOptions(options) {
        let moduleData = options.map(y => {
            const opt = y;
            let res = opt.semesters.map(s => {
                return s.modules.map(x => x.module);
            });
            return [].concat(...res);
        });
        return [].concat(...moduleData);
    }

    setRoadmap(year, semester, module) {
        if (!(year in this.roadmap)) {
            this.roadmap[year] = {};
        }
        this.roadmap[year][semester - 1] = module;
        this.renderRoadmap(this.options)

    }


    selectModule(e) {
        //Get the object and the data id;
        const module = $(e.target).closest('.module');
        const moduleId = module.attr('data-module');

        //Check if selected module is valid;

        if (moduleId == null) {
            console.error("Invalid module clicked");
            return;
        }

        //Find the list.
        const card = module.closest('.card');
        const year = card.attr('data-year');
        const semester = card.attr('data-semester');

        let selectModule = this;
        let moduleObj = this.flatOptions.filter(m => m.id === moduleId);

        if (moduleObj.length > 0) {
            moduleObj = moduleObj[0];

            var controller = new ApiController();

            controller.get('module/GetModule', moduleObj.id).then(function (resultModule) {
                var validateModel = Validate(resultModule);

                if (!validateModel.status) {
                    ErrorMessage(validateModel.errors);
                } else {

                    selectModule.setRoadmap(year, semester, moduleObj);

                    //Remove selected state from current
                    card.find('.module-list .module.selected').removeClass('selected');

                    //Set selected state to current
                    module.addClass('selected');

                    //Enable next card
                    card.next().removeClass('disabled');
                }
            });
        } else {
            console.error("Invalid module clicked");

        }

    }


    renderModuleButton(moduleData) {

        moduleData = ('module' in moduleData ? moduleData.module : moduleData);
        let module = document.createElement('div');
        module.classList.add('module');
        module.setAttribute('data-module', moduleData.id);

        let content = document.createElement('div');
        content.classList.add('content');

        let title = document.createElement('h3');
        title.innerText = moduleData.name ?? "???";

        let ec = document.createElement('span');
        ec.classList.add('ec');
        ec.innerText = moduleData.ec ?? 0;

        content.append(title, ec);

        let info = document.createElement('div');
        info.setAttribute('class', 'tooltip');

        let infoimage = document.createElement('img');
        infoimage.setAttribute('src', 'https://image.flaticon.com/icons/svg/66/66788.svg');
        infoimage.setAttribute('class', 'infoimage');
        infoimage.setAttribute('id', 'infoimage');


        let infotext = document.createElement('span');
        infotext.innerText = "Competenties:";

        infoimage.addEventListener("mouseover", function () {

            new ApiController().get('module/GetModuleRequirements/' + moduleData.id).then(
                (data) => {
                    data.forEach(function (item) {

                        infotext.innerHTML += " " + item['competence']['competenceName'] + ', </br>';
                    });
                });
        }, {once: true});

        infotext.setAttribute('class', 'tooltiptext');
        infotext.setAttribute('id', 'tooltiptext');


        info.append(infotext);
        info.append(infoimage);


        module.append(content, info);


        module.addEventListener("click", (e) => this.selectModule(e));

        return module;
    }

    renderSemesterCard(year, semester, modules, children) {
        let card = document.createElement('section');
        card.classList.add('card');

        if (!(children === 0)) {
            card.classList.add('disabled');
        }
        card.setAttribute("data-year", year);
        card.setAttribute("data-semester", semester);

        let h2 = document.createElement('h2');
        h2.innerText = `Jaar ${year}, Semester ${semester}`;

        let list = document.createElement('div');
        list.classList.add('module-list');

        card.append(h2, list);

        const options = modules.map((m) => this.renderModuleButton(m));
        list.append(...options);

        return card;
    }


    renderRoadmapSegment(year, semesters) {
        semesters = semesters.semesters;

        const segment = document.createElement('div');
        segment.classList.add('semester');

        const tag = document.createElement('span');
        tag.classList.add('tag');
        tag.innerText = `Jaar ${year}`;

        segment.append(tag);

        let sNo = 0;
        const modules = semesters.map((s) => {
            if (year in this.roadmap && sNo in this.roadmap[year] && this.roadmap[year][sNo] !== null) {
                sNo++;
                return this.renderModuleButton(this.roadmap[year][sNo - 1]);
            } else {
                sNo++;
                return this.renderModuleButton({});
            }
        });

        segment.append(...modules);

        return segment;
    }

    renderModules(options) {
        let element = document.getElementById(this.optionsContainerId);
        if (element != null) {
            element.innerHTML = "";

            const cards = [];
            options.forEach((year) => {
                let semesterNo = 1;
                year.semesters.forEach((semester) => {
                    cards.push(this.renderSemesterCard(parseInt(year.year, 10), semesterNo, semester.modules, cards.length));
                    semesterNo++;
                });
            });
            element.append(...cards);
        } else {
            console.error("Could not append modules to element.");

        }
    }

    renderRoadmap(options) {
        let element = document.getElementById(this.roadmapContainerId);
        if (element != null) {
            element.innerHTML = "";

            const segments = [];
            options.forEach((year) => {
                segments.push(this.renderRoadmapSegment(year.year, year));
            });
            element.append(...segments);
        } else {
            console.error("Could not append roadmap to element.");

        }
    }
    
    flattenRoadMap(roadmap) {
        const rm = Object.keys(roadmap)
            .map((r) => {
                let res = Object.keys(roadmap[r])
                    .map(y => {
                         return roadmap[r][y].id;
                    });
                return [].concat(...res)
            });
        return [].concat(...rm);
    }

    handleSaveRoadmap() {
        //change for update
        let API = new ApiController();

        API.get('Student')
            .then((me) => {
                const user = me;
                const RoadMapObject = this.flattenRoadMap(this.roadmap);
                console.log("ME", user);
                if(user.roadMapId == null) {
                    // Should be impossible
                    API.post('RoadMap', RoadMapObject).then((roadmap) => {
                        if(roadmap.id != null) {
                            user.roadmapid = roadmap.id;
                            API.put('Student', user.id, user);
                        }
                    });
                } else {
                    API.put('RoadMap', user.roadMap.id, RoadMapObject)
                        .then((rm) => {
                            console.log("PUT", rm);
                        })
                }


                
                
        });
    }
}