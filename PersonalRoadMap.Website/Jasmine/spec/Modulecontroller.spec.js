describe('ModuleController', function () { 
    beforeAll(function () {
        ModuleController = new ModuleController();
    });
    it("Select box should contain options", async function () {
        //Act
        await ModuleController.__init()
        //Assert         
        expect(document.getElementById('CompetencesList').options).not.toBe(null);
    })
    it("Module overview should contain childs", async function () {  
        //Assert
        expect(document.getElementById('ModuleOverviewCards').children).not.toBe(null);
    })

});
