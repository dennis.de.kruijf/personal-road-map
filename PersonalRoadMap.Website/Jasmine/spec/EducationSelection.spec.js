describe('EducationSelection', function () {
    beforeAll(function () {
        EduSelect = new EducationSelection();
    });
    
    it("Should be able to initiate", async function() {
        //Arrange
        const optionsContainerID = "testcontainer";
        const roadmapContainerID = "testroute";
        const saveButtonID = "testSaveRoadMapButton";
        
        //Act
        const schoolYears = 4;

        await EduSelect.__init(optionsContainerID, roadmapContainerID, saveButtonID);
        
        //Assert
        expect(EduSelect.options.length).toBe(schoolYears);
        expect(EduSelect.flatOptions.length).not.toBe(0);
        expect(Object.keys(EduSelect.roadmap).length).toBe(0);
    })
});